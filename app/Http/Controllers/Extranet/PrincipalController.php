<?php

namespace App\Http\Controllers\Extranet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrincipalController extends Controller
{

  public function principal(Request $request)
  {
  	//$repuestos = Repuesto::where('activo','1')->paginate(3);
  	//if ($request->ajax()) {
	//	$view = view('repuestos.repuesto',compact('repuestos'))->render();
    //    return response()->json(['html'=>$view]);
    //}
    //return view('repuestos.principal',compact('repuestos'));
    return view('accesorios.principal');
  }

  public function show($accesorioId)
  {
  	$accesorio = Accesorio::where('id','=',$accesorioId)->where('activo','1')->first();
  	if (!$accesorio) {
  		return redirect()->route('accesorios.principal');
  	}
    return view('accesorios.show',compact('accesorio'));
  }

  public function form($accesorioId)
  {
    $accesorio = Accesorio::where('id','=',$accesorioId)->where('activo','1')->first();
    if (!$accesorio) {
      return redirect()->route('accesorios.principal');
    }
    return view('accesorios.form',compact('accesorio'));
  }

}