<?php

namespace App\Http\Controllers\Flotas;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Orden;
use App\Cliente;
use App\Version;
use App\SalerUser;
use Excel;
use DB;

class DashboardController extends Controller
{

  public function main(Request $request)
  {
    $ordenes = Orden::orderBy('created_at', 'desc')->get();

    return view('flotas.dashboard.main', compact('ordenes'));
  }

  public function ordenes(Request $request)
  {
      $user = Auth::user();

      if($user->role_id == 2){
          $ordenes = Orden::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

          return view('flotas.dashboard.user.ordenes', compact('ordenes'));
      }

      if($user->role_id == 3){
            $sales_id = [];
            $sale_users = SalerUser::where('saler_id', $user->id)->get();

            foreach ($sale_users as $sale_user){
                $sales_id[] = $sale_user->user_id;
            }

          $ordenes = Orden::whereIn('user_id', $sales_id)->orderBy('created_at', 'desc')->get();

          return view('flotas.dashboard.user.ordenes', compact('ordenes'));
      }

      $ordenes = Orden::orderBy('created_at', 'desc')->get();

      return view('flotas.dashboard.admin.ordenes', compact('ordenes'));

  }

  public function listVersion(Request $request)
  {
  	if($request->has('modelo_id')){
	  	$version = Version::where('modelo_id', $request->modelo_id)->orderBy('nombre')->get();

	  	return json_encode($version);
  	}
  }

  public function download($pdf){
  	$file = decrypt($pdf);

  	if (file_exists($file)) {
  		return response()->download(public_path() .'/'. $file);
  	}
  }

  public function clientes()
  {
  	$clientes = Cliente::orderBy('created_at', 'desc')->get();
  	return view('flotas.dashboard.clientes', compact('clientes'));
  }

  public function downloadOrdenesXls(){

    $ordenes  = Orden::orderBy('created_at', 'desc')
                    ->get(['id', 'created_at', 'contacto_apellidos', 'cantidad', 'modelo_id', 'precio_lista_dolares']);

    $header  = array('ORDEN', 'FECHA', 'CLIENTE', 'UNIDADES',  'MODELO',  'PRECIO ($)');

    $total_ordenes = count($ordenes) + 1;

    Excel::create('Ordenes', function($excel)  use( $ordenes, $header, $total_ordenes ){

      // Set the title
      $excel->setTitle('Ordenes');

      // Chain the setters
      $excel->setCreator('acarbaro')
            ->setCompany('acarbaro');

      // Call them separately
      // $excel->setDescription('A demonstration to change the file properties');

      $excel->sheet('Ordenes', function($sheet) use( $ordenes, $header, $total_ordenes ){
        $sheet->setSize(array(
            'A1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'B1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'C1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'D1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'E1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'F1' => array(
                'width'     => 30,
                'height'    => 20
            )
        ));
        $sheet->freezeFirstRow();
        $sheet->row(1, $header);
        $sheet->fromArray($ordenes, null, 'A2', false, false);
        $sheet->setBorder('A1:F'. $total_ordenes, 'thin');
        $sheet->cells('A1:F1', function($cells) {
          $cells->setFont(array(
              'family'     => 'Calibri',
              'size'       => '13',
              'bold'       =>  true
          ));
          $cells->setAlignment('center');
        });
        $sheet->cells('A2:F'. $total_ordenes, function($cells) {
          $cells->setFont(array(
              'family'     => 'Calibri',
              'size'       => '12',
          ));
          $cells->setAlignment('center');
        });
      });

    })->download('xlsx');
  }


  public function downloadClientesXls(){
    $clientes = Cliente::orderBy('created_at', 'desc')
                        ->get(['id', 'dni', 'nombres', 'apellidos', 'telefono', 'email', 'fuente_contacto']);

    $header  = array('CLIENTE', 'DNI', 'NOMBRES', 'APELLIDOS', 'TELEFONO', 'EMAIL', 'FUENTE CONTACTO');

    $total_clientes = count($clientes) + 1;

    Excel::create('Clientes', function($excel) use( $clientes, $header, $total_clientes ){

      // Set the title
      $excel->setTitle('Ordenes');

      // Chain the setters
      $excel->setCreator('acarbaro')
            ->setCompany('acarbaro');

      $excel->sheet('Clientes', function($sheet) use( $clientes, $header, $total_clientes ){
        $sheet->setSize(array(
            'A1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'B1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'C1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'D1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'E1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'F1' => array(
                'width'     => 30,
                'height'    => 20
            ),
            'G1' => array(
                'width'     => 30,
                'height'    => 20
            )
        ));
        $sheet->freezeFirstRow();
        $sheet->row(1, $header);
        $sheet->fromArray($clientes, null, 'A2', false, false);
        $sheet->setBorder('A1:G'. $total_clientes, 'thin');
        $sheet->cells('A1:G1', function($cells) {
          $cells->setFont(array(
              'family'     => 'Calibri',
              'size'       => '13',
              'bold'       =>  true
          ));
          $cells->setAlignment('center');
        });
        $sheet->cells('A2:G'. $total_clientes, function($cells) {
          $cells->setFont(array(
              'family'     => 'Calibri',
              'size'       => '12',
          ));
          $cells->setAlignment('center');
        });
      });

    })->download('xlsx');

  }


}