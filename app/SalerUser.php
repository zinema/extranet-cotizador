<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalerUser extends Model
{
    protected $table = 'saler_users';
}
