@extends('flotas.layout.master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="header">
				<div class="pull-right">
					<a  href="{{ route('flotas.dashboard.descargar.clientes') }}" class="btn btn-info btn-fill btn-wd">
						Descargar
					</a>
				</div>
				<h4 class="title">Últimas clientes</h4>
				<p class="category">Here is a subtitle for this table</p>
			</div>
			<div class="content table-responsive table-full-width">
				@if(count($clientes)>0)
								<table class="table">
									<thead> 
										<tr> 
											<th>Cliente</th> 
											<th>Dni</th> 
											<th>Nombres</th>
											<th>Apellidos</th>
											<th>Telefono</th>
											<th>Email</th>
											<th>Fuente Contacto</th>
											<!-- <th>Descargar</th> -->
										</tr>
									</thead> 
									<tbody> 
											@foreach($clientes as $cliente)
											<tr> 
												<td># {{$cliente->id}}</td> 
												<td>{{ $cliente->dni }}</td> 
												<td>{{ $cliente->nombres }}</td>
												<td>{{ $cliente->apellidos }}</td>
												<td>{{ $cliente->telefono }}</td>
												<td>{{ $cliente->email }}</td>
												<td>{{ $cliente->fuente_contacto}}</td>
											</tr> 
											@endforeach
									</tbody> 
								</table>
						@else
				<h4>No hay clientes</h4>
				@endif
			</div>
		</div>
	</div>
</div>
@stop