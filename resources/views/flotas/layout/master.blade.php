<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="http://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Volkswagen</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="canonical" href=""/>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <!-- Bootstrap core CSS     -->
    <link href="{{asset('static-cotizador/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="{{asset('static-cotizador/css/light-bootstrap-dashboard.css')}}" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{asset('static-cotizador/css/demo.css')}}" rel="stylesheet"/>
    <!--     Fonts and icons     -->
    <link href="{{asset('static-cotizador/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('static-cotizador/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
    @yield('css')
</head>
<body>
    <div class="wrapper">
      @include('flotas.layout.nav')
        @include('flotas.layout.sidebar')
        <div class="main-panel">
          <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        @include('flotas.layout.footer')
    </div>
</div>
</body>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="{{asset('static-cotizador/js/jquery.min.js')}}"></script>
<script src="{{asset('static-cotizador/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('static-cotizador/js/bootstrap.min.js')}}"></script>
<!--  Forms Validations Plugin -->
<script src="{{asset('static-cotizador/js/jquery.validate.min.js')}}"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{asset('static-cotizador/js/moment.min.js')}}"></script>
<!--  Date Time Picker Plugin is included in this js file -->
<script src="{{asset('static-cotizador/js/bootstrap-datetimepicker.js')}}"></script>
<!--  Select Picker Plugin -->
<script src="{{asset('static-cotizador/js/bootstrap-selectpicker.js')}}"></script>
<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="{{asset('static-cotizador/js/bootstrap-checkbox-radio-switch-tags.js')}}"></script>
<!--  Charts Plugin -->
<script src="{{asset('static-cotizador/js/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('static-cotizador/js/bootstrap-notify.js')}}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{asset('static-cotizador/js/sweetalert2.js')}}"></script>
<!-- Vector Map plugin -->
<script src="{{asset('static-cotizador/js/jquery-jvectormap.js')}}"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<!-- Wizard Plugin    -->
<script src="{{asset('static-cotizador/js/jquery.bootstrap.wizard.min.js')}}"></script>
<!--  Bootstrap Table Plugin    -->
<script src="{{asset('static-cotizador/js/bootstrap-table.js')}}"></script>
<!--  Plugin for DataTables.net  -->
<script src="{{asset('static-cotizador/js/jquery.datatables.js')}}"></script>
<!--  Full Calendar Plugin    -->
<script src="{{asset('static-cotizador/js/fullcalendar.min.js')}}"></script>
<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="{{asset('static-cotizador/js/light-bootstrap-dashboard.js')}}"></script>
<!--   Sharrre Library    -->
<script src="{{asset('static-cotizador/js/jquery.sharrre.js')}}"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('static-cotizador/js/demo.js')}}"></script>

<script type="text/javascript">
   $(document).ready(function(){

       demo.initDashboardPageCharts();
       demo.initVectorMap();

       $.notify({
           icon: 'pe-7s-bell',
           message: "<b>Light Bootstrap Dashboard PRO</b> - forget about boring dashboards."

       },{
        type: 'warning',
        timer: 4000
    });

   });
</script>
@yield('js')
</html>