<?php

namespace App\Http\Controllers\Flotas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SalerUser;

class SalerUserController extends Controller
{
    public function index(){

        return view('vendor.voyager.saler-users.browse');
    }

    public function new(){
        return view('vendor.voyager.saler-users.edit-add');
    }

    public function store(Request $request){
        $saler_user = new SalerUser;
        $saler_user->saler_id = $request->saler;
        $saler_user->user_id = $request->user;
        $saler_user->save();

        return back();
    }

    public function delete($id){

        $saler_user = SalerUser::find(decrypt($id));
        $saler_user->delete();

        return back();
    }
}
