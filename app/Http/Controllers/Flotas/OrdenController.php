<?php

namespace App\Http\Controllers\Flotas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use PDF;
use App\Orden;
use App\Cliente;
use App\Aditional;
use App\Version;
use App\Modelo;
use Carbon\Carbon;


class OrdenController extends Controller
{
    public function index(){

    	$modelos = Modelo::orderBy('nombre')->get();

    	// $version = Version::orderBy('nombre')->get();

    	return view('flotas.dashboard.orden', compact('modelos'));
    }

    public function storeOrdenVendedor(Request $request){
    	if($request->filled(['nombres','apellidos','email','telefono'])){
			$id = DB::table('ordenes')->insertGetId(
			   	[
			   	 'user_id'            => $request->user_id,
			     'vendedor_nombres'   => $request->nombres, 
			     'vendedor_apellidos' => $request->apellidos,
			     'vendedor_email'	  => $request->email,
			     'vendedor_telefono'  => $request->telefono,
			     'created_at'         => Carbon::now()
			    ]
			);    		

			return json_encode($id);
    	}
    }

    public function storeOrdenCliente(Request $request){
		DB::table('ordenes')
		->where('id', $request->orden_id)
		->update(
		   	[
		     'contacto_nombres'   => $request->nombres_cliente, 
		     'contacto_apellidos' => $request->apellidos_cliente,
		     'contacto_email'	  => $request->email,
		     'dni_cliente'	  	  => $request->dni,
		     'contacto_celular'   => $request->telefono,
		     'contacto_fuente'    => $request->fuente_contacto_cliente,
		    ]
		);    		

		if($request->filled(['dni', 'nombres_cliente', 'apellidos_cliente', 'email', 'telefono', 'fuente_contacto_cliente'])){
			$cliente = new Cliente;
			$cliente->dni =	$request->dni;
			$cliente->nombres =	$request->nombres_cliente;
			$cliente->apellidos =	$request->apellidos_cliente;
			$cliente->telefono =	$request->telefono;
			$cliente->email =	$request->email;
			$cliente->fuente_contacto =	$request->fuente_contacto_cliente;
			$cliente->orden_id =	$request->orden_id;
			$cliente->save();
		}

    }

    public function storeOrdenVehiculo(Request $request){

    	DB::table('ordenes')
		->where('id', $request->orden_id)
		->update(
		   	[
		     'modelo_id'   	  => intval($request->modelo), 
		     'version_id'  	  => intval($request->version),
		     'cantidad'	   	  => intval($request->cantidad),
		     'color'   	   	  => $request->color,
		     'fabricacion'    => $request->fabricacion,
		     'anio'    		  => $request->year_modelo,
		     'combustible'    => $request->combustible,
		    ]
		);    
    }

    public function storeOrdenAdicional(Request $request){

    	if($request->filled('orden_id')){
	    	foreach ($request->adicional_nombre as $k => $val) {
		     	$adicional 			 = new Aditional;
		    	$adicional->producto = $request->adicional_nombre[$k];
		    	$adicional->precio   = $request->adicional_precio[$k];
		    	$adicional->orden_id = $request->orden_id;
		    	$adicional->save();
	    	}    		
    	}

    }

    public function storeOrdenPrecio(Request $request){
    	DB::table('ordenes')
		->where('id', $request->orden_id)
		->update(
		   	[
		     'precio_lista_dolares'  	=> $request->precio_lista_dls, 
		     'bono_dolares'  	  	 	=> $request->bono_dls,
		     'precio_sugerido_dolares'  => $request->precio_sugerido_dls,
		     'precio_lista_soles'   	=> $request->precio_lista_sol,
		     'bono_soles'    		    => $request->bono_sol,
		     'precio_lista_soles'       => $request->precio_sugerido_sol,
		     'flete'    				=> $request->flete,
		     'validez_oferta'    		=> $request->validez_oferta,
		    ]
		);    
    }

    public function downloadPdf(Request $request){
    	if($request->has('orden_id')){
	    	$orden = Orden::find($request->orden_id);
			$pdf = PDF::loadView('pdf.cotizador', compact('orden'));

			$name_file = 'cotizacion_'.time().'_'.$orden->id.'.pdf';
			$orden->pdf = $name_file;
			$orden->save();

			$pdf->save(public_path().'/'.$name_file);

			return $pdf->download($name_file);    		
    	}
    }

}
