<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'MainController@home')->name('main.home');

/** Rutas Extranet **/
Route::get('/extranet', 'Extranet\DashboardController@main')->name('extranet.dashboard.main')->middleware('auth');

/** Rutas Flotas **/
Route::group(['prefix' => 'cotizador', 'middleware' => 'auth'], function () {
	Route::get('/', 'Flotas\DashboardController@main')->name('flotas.dashboard.main');
	Route::get('/ordenes', 'Flotas\DashboardController@ordenes')->name('flotas.dashboard.ordenes');
	Route::get('/descargar_ordenes', 'Flotas\DashboardController@downloadOrdenesXls')->name('flotas.dashboard.descargar.ordenes');
	Route::get('/clientes', 'Flotas\DashboardController@clientes')->name('flotas.dashboard.clientes');
	Route::get('/descargar_clientes', 'Flotas\DashboardController@downloadClientesXls')->name('flotas.dashboard.descargar.clientes');
	Route::get('/modelo-versiones', 'Flotas\DashboardController@listVersion')->name('flotas.listar.versiones');
	Route::get('/download/{pdf}', 'Flotas\DashboardController@download')->name('cotizaciones.download.pdf');
	Route::get('/orden', 'Flotas\OrdenController@index')->name('flotas.new.orden');
    Route::get('/orden/download-pdf', 'Flotas\OrdenController@downloadPdf')->name('flotas.download.pdf');
    Route::post('/orden/new-orden-vendedor', 'Flotas\OrdenController@storeOrdenVendedor')->name('flotas.store.orden');
	Route::post('/orden/new-orden-cliente', 'Flotas\OrdenController@storeOrdenCliente')->name('flotas.store.cliente');
	Route::post('/orden/new-orden-vehiculo', 'Flotas\OrdenController@storeOrdenVehiculo')->name('flotas.store.vehiculo');
	Route::post('/orden/new-orden-adicional', 'Flotas\OrdenController@storeOrdenAdicional')->name('flotas.store.adicional');
	Route::post('/orden/new-orden-precio', 'Flotas\OrdenController@storeOrdenPrecio')->name('flotas.store.precio');
	Route::get('/logout', function () {
        Auth::logout();
        return redirect()->route('voyager.login');
    })->name('logout');
});



Route::get('/login', function () {
    return redirect()->route('voyager.login');
})->name('login');


Route::group(['prefix' => '/admin'], function () {
    Voyager::routes();
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('saler_users', ['uses' => 'Flotas\SalerUserController@index', 'as' => 'saler_users']);
    Route::get('new/saler_users', ['uses' => 'Flotas\SalerUserController@new', 'as' => 'new.saler_users']);
    Route::post('salers-users', ['uses' => 'Flotas\SalerUserController@store', 'as' => 'store.saler_users']);
    Route::get('salers-users/delete/{id}', ['uses' => 'Flotas\SalerUserController@delete', 'as' => 'delete.saler_users']);
});