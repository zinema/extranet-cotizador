<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><span style="font-weight: bold;">EXTRANET</span><img src="https://vignette4.wikia.nocookie.net/logopedia/images/1/1a/Volkswagen_Logo.png"></a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-with-icons">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<div>
							<img src="images/avatar.png" alt="" style="width: 30px;">
							<p>Sr. Van Helsing</p>
						</div>
						<p class="hidden-md hidden-lg">
							More
							<b class="caret"></b>
						</p>
					</a>
					<ul class="dropdown-menu dropdown-with-icons">
						<li>
							<a href="#">
								<i class="pe-7s-mail"></i> Messages
							</a>
						</li>
						<li>
							<a href="#">
								<i class="pe-7s-help1"></i> Help Center
							</a>
						</li>
						<li>
							<a href="#">
								<i class="pe-7s-tools"></i> Settings
							</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">
								<i class="pe-7s-lock"></i> Lock Screen
							</a>
						</li>
						<li>
							<a href="{{ route('logout') }}" class="text-danger">
								<i class="pe-7s-close-circle"></i>
								Log out
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<form class="navbar-form navbar-left navbar-search-form" role="search">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
					<input type="text" value="" class="form-control" placeholder="Buscar...">
				</div>
			</form>
		</div>
	</div>
</nav>