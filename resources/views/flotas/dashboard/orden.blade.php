@extends('flotas.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <?php $user = Auth::user(); ?>
            <div id="main">
                <ul class="nav nav-tabs">
                    <li id="li-vendedor" class="active"><a data-toggle="tab" href="#vendedor">Vendedor</a></li>
                    <li id="li-cliente"><a data-toggle="tab" href="#cliente">Cliente</a></li>
                    <li id="li-vehiculos"><a data-toggle="tab" href="#vehiculos">Vehiculos</a></li>
                    <li id="li-adicional"><a data-toggle="tab" href="#adicional">Información Adicional</a></li>
                    <li id="li-precio"><a data-toggle="tab" href="#precio">Precio de Venta</a></li>
                    <li id="li-exportar"><a data-toggle="tab" href="#exportar">Exportar</a></li>
                </ul>
                <div class="tab-content">
                    <div id="vendedor" class="tab-pane active">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombres">Nombres</label>
                                    <input type="text" class="form-control" id="nombres" name="nombres"
                                           value="{{ $user->name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="apellidos">Apellidos</label>
                                    <input type="text" class="form-control" id="apellidos" name="apellidos"
                                           value="{{ $user->apellido }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Telefono</label>
                                    <input type="text" class="form-control" id="telefono" name="telefono"
                                           value="{{ $user->telefono }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success" id="continuar_vendedor">Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cliente" class="tab-pane">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombres">Nombres</label>
                                    <input type="text" class="form-control" id="nombres_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="apellidos">Apellidos</label>
                                    <input type="text" class="form-control" id="apellidos_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Dni</label>
                                    <input type="email" class="form-control" id="dni_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Celular</label>
                                    <input type="text" class="form-control" id="celular_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Email</label>
                                    <input type="text" class="form-control" id="email_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Fuente de contacto</label>
                                    <input type="text" class="form-control" id="fuente_contacto_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Urgencia de compra</label>
                                    <input type="text" class="form-control" id="urgencia_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" id="continuar_cliente">Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="vehiculos" class="tab-pane">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Modelos</label>
                                    <select id="modelo" id="modelo" class="form-control">
                                        <option disabled>Seleccionar Modelo</option>
                                        @foreach($modelos as $modelo)
                                            <option value="{{ $modelo->id }}">{{ $modelo->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="div-version"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="text" class="form-control" id="cantidad">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="codigo_modelo">Codigo Modelo</label>
                                    <input type="text" class="form-control" id="codigo_modelo">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fabricacion">Fabricación</label>
                                    <input type="text" class="form-control" id="fabricacion">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year_modelo">Año modelo</label>
                                    <input type="text" class="form-control" id="year_modelo">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="combustible">Combustible</label>
                                    <input type="text" class="form-control" id="combustible">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" id="continuar_vehiculo">
                                            Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="adicional" class="tab-pane">
                        <div class="row">
                            <div class="row grupo_campos">
                                <div class="row campos_adicional">
                                    <div class="col-md-5">
                                        <label for="fabricacion">Nombre</label>
                                        <input type="text" class="form-control" id="nombre_adicional"
                                               name="adicional_nombres[]">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="fabricacion">Precio</label>
                                        <input type="text" class="form-control" id="precio_adicional"
                                               name="adicional_precios[]">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-simple btn-wd eliminar_campo">Eliminar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 0 !important;">
                                <div class="text-right">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-info agregar_campo">Agregar Campo &nbsp; <span
                                                style="font-size:16px; font-weight:bold;">+ </span></button>
                                </div>
                            </div>
                            <div class="row" style="margin: 0 !important;">
                                <div class="text-right">
                                    <label>&nbsp;</label>
                                    <button type="submit" class="btn btn-success" id="continuar_adicional">Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="precio" class="tab-pane">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="precio_lista_dls">Precio de Lista ($$)</label>
                                    <input type="text" class="form-control" id="precio_lista_dls">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bono_dls">Bono ($$)</label>
                                    <input type="text" class="form-control" id="bono_dls">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="precio_sugerido_dls">Precio Sugerido ($$)</label>
                                    <input type="text" class="form-control" id="precio_sugerido_dls">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="precio_lista_sol">Precio de Lista (S/)</label>
                                    <input type="text" class="form-control" id="precio_lista_sol">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bono_sol">Bono (S/)</label>
                                    <input type="text" class="form-control" id="bono_sol">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="precio_sugerido_sol">Precio Sugerido (S/)</label>
                                    <input type="text" class="form-control" id="precio_sugerido_sol">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flete">Flete (opcional)</label>
                                    <select id="flete">
                                        <option value="1">Flete 1</option>
                                        <option value="2">Flete 2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="validez_oferta">Valides de la oferta</label>
                                    <input type="text" class="form-control" id="validez_oferta">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" id="continuar_precio">Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="exportar" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="GET" action="{{ route('flotas.download.pdf') }}">
                                    <input type="hidden" name="orden_id" id="orden_id">
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                    <button type="submit" id="exportar" class="btn btn-info">Exportar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        var orden_id = '';

        $("#continuar_vendedor").click(function () {
            var token = "{{ csrf_token() }}";
            var nombres = $("#nombres").val();
            var apellidos = $("#apellidos").val();
            var email = $("#email").val();
            var telefono = $("#telefono").val();
            var user_id = $("#user_id").val();

            $.ajax({
                url: "{{ route('flotas.store.orden') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    nombres: nombres,
                    apellidos: apellidos,
                    email: email,
                    telefono: telefono,
                    user_id: user_id
                },
                success: function (d) {
                    if (d) {
                        $("#orden_id").val(d);
                        $('#vendedor').removeClass('active');
                        $('#cliente').addClass('active');
                        $('#li-vendedor').removeClass('active');
                        $('#li-cliente').addClass('active');
                    } else {
                        swal("Error", "Completa los campos requeridos.", "error");
                    }
                }
            });
        });

        $("#continuar_cliente").click(function () {
            var token = "{{ csrf_token() }}";
            var nombres_cliente = $("#nombres_cliente").val();
            var apellidos_cliente = $("#apellidos_cliente").val();
            var dni = $("#dni_cliente").val();
            var telefono = $("#celular_cliente").val();
            var email = $("#email_cliente").val();
            var fuente_contacto_cliente = $("#fuente_contacto_cliente").val();
            var urgencia_cliente = $("#urgencia_cliente").val();
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.cliente') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    nombres_cliente: nombres_cliente,
                    apellidos_cliente: apellidos_cliente,
                    dni: dni,
                    telefono: telefono,
                    email: email,
                    fuente_contacto_cliente: fuente_contacto_cliente,
                    urgencia_cliente: urgencia_cliente,
                    orden_id: orden_id
                },
                success: function (d) {
                    console.log(d);
                    $('#cliente').removeClass('active');
                    $('#vehiculos').addClass('active');
                    $('#li-cliente').removeClass('active');
                    $('#li-vehiculos').addClass('active');
                }
            });
        });

        $("#modelo").change(function () {
            var modelo_id = $("#modelo").val();
            var version = $("#div-version");
            var select = '';
            $.ajax({
                url: "{{ route('flotas.listar.versiones') }}",
                type: 'GET',
                data: {modelo_id: modelo_id},
                success: function (response) {
                    var versions = JSON.parse(response);
                    select += '<label for="version">Version</label>';
                    select += '<select name="version" class="form-control input-sm " required id="version" >';
                    $.each(versions, function (index, version) {
                        select += '<option value=" ' + version.id + ' ">' + version.nombre + '</option>';
                    })
                    select += '</select>';

                    console.log(select);
                    version.html(select);
                }
            });
        });

        $("#continuar_vehiculo").click(function () {
            var token = "{{ csrf_token() }}";
            var modelo = $("#modelo").val();
            var version = $("#version").val();
            var cantidad = $("#cantidad").val();
            var codigo_modelo = $("#codigo_modelo").val();
            var fabricacion = $("#fabricacion").val();
            var year_modelo = $("#year_modelo").val();
            var combustible = $("#combustible").val();
            var color = $("#color").val();
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.vehiculo') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    modelo: modelo,
                    version: version,
                    cantidad: cantidad,
                    codigo_modelo: codigo_modelo,
                    fabricacion: fabricacion,
                    year_modelo: year_modelo,
                    combustible: combustible,
                    color: color,
                    orden_id: orden_id
                },
                success: function (d) {
                    console.log(d);
                    $('#vehiculos').removeClass('active');
                    $('#adicional').addClass('active');
                    $('#li-vehiculos').removeClass('active');
                    $('#li-adicional').addClass('active');
                }
            });
        });


        $("#continuar_adicional").click(function () {
            var adicional_nombre = [];
            var adicional_precio = [];
            var count_n = 0;
            var count_p = 0;
            var t = 0;
            $('input[name^="adicional_nombres"]').each(function () {
                adicional_nombre[count_n++] = $(this).val();
            });

            $('input[name^="adicional_precios"]').each(function () {
                adicional_precio[count_p++] = $(this).val();
            });

            var token = "{{ csrf_token() }}";
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.adicional') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    adicional_nombre: adicional_nombre,
                    adicional_precio: adicional_precio,
                    orden_id: orden_id
                },
                success: function (d) {
                    $('#adicional').removeClass('active');
                    $('#exportar').addClass('active');
                    $('#li-adicional').removeClass('active');
                    $('#li-exportar').addClass('active');
                }
            });
        });

        $("#continuar_precio").click(function () {
            var token = "{{ csrf_token() }}";
            var precio_lista_dls = $("#precio_lista_dls").val();
            var bono_dls = $("#bono_dls").val();
            var precio_sugerido_dls = $("#precio_sugerido_dls").val();
            var precio_lista_sol = $("#precio_lista_sol").val();
            var bono_sol = $("#bono_sol").val();
            var precio_sugerido_sol = $("#precio_sugerido_sol").val();
            var flete = $("#flete").val();
            var validez_oferta = $("#validez_oferta").val();
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.precio') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    precio_lista_dls: precio_lista_dls,
                    bono_dls: bono_dls,
                    precio_sugerido_dls: precio_sugerido_dls,
                    precio_lista_sol: precio_lista_sol,
                    bono_sol: bono_sol,
                    precio_sugerido_sol: precio_sugerido_sol,
                    flete: flete,
                    validez_oferta: validez_oferta,
                    orden_id: orden_id
                },
                success: function (d) {
                    $('#precio').removeClass('active');
                    $('#exportar').addClass('active');
                    $('#li-precio').removeClass('active');
                    $('#li-exportar').addClass('active');
                }
            });
        });

    </script>



    <script>
        $(document).ready(function () {
            var max_fields = 10;
            var wrapper = $(".grupo_campos");
            var add_button = $(".agregar_campo");

            var x = 1;
            $(add_button).click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    var bloque = '<div class="row campos_adicional">' +
                        '<div class="col-md-5">' +
                        '<label for="fabricacion">Nombre</label>' +
                        '<input type="text" class="form-control" id="nombre_adicional" name="adicional_nombres[]">' +
                        '</div>' +
                        '<div class="col-md-5">' +
                        '<label for="fabricacion">Precio</label>' +
                        '<input type="text" class="form-control" id="precio_adicional" name="adicional_precios[]">' +
                        '</div>' +
                        '<div class="col-md-2">' +
                        '<button class="btn btn-simple btn-wd eliminar_campo">Eliminar</button>' +
                        '</div>' +
                        '</div>';
                    $(wrapper).append(bloque);
                }
                else {
                    alert('Llegaste al límite de inputs');
                }
            });

            $(wrapper).on("click", ".eliminar_campo", function (e) {
                e.preventDefault();
                $(this).parent('div').parent('.campos_adicional').remove();
                x--;
            })
        });
    </script>


@stop