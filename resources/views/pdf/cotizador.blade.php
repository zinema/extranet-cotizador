<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
	</title>

	<style>
		body{
			width: 800px;
		}
		#encabezado{
			padding: 0;
			width: 200px;
			position: relative;
		}

		#encabezado .texto{
			font-size: 24px;
			color: #666;
			font-weight: bold;
			width: 50px;
			width: 100px;
		}

		#encabezado .imagen{
			position: absolute;
			top: 50%;
			left: 100px;
			margin-top: -20px;
		}

		#encabezado img{
			width: 40px;
			height: 40px;
		}

		#consecionario{
			display: inline-block;
			height: 200px;
			width: inherit;
		}

		.consecionario-texto{
			float: left;
			top: 50%;
			margin-top: -197px;
			position: absolute;
			margin-bottom: 0;
		}

		.consecionario-texto .nombre-consecionario{
			font-size: 36px;
			font-weight: bold;
			color: #2f2f2f;
			line-height: 0px;
			margin-bottom: 30px;
		}

		.consecionario-texto .numero{
			font-size: 20px;
			margin: 0;
		}

		.consecionario-imagen{
			float: right;
			left: 350px;
			position: absolute;
		}

		.consecionario-imagen img{
			width: 400px;
		}

		.consecionario-imagen {
			float: right;
			left: 350px;
			position: absolute;
			top: 50%;
			margin-top: -197px;
		}

		.input{
			color: #808390;
			font-size: 20px;
			font-weight: bold;
			margin-bottom: 10px;
		}

		.input-informacion{
			color: #264B41;
			font-size: 22px;
			margin: 0;
		}

		.informacion{
			margin-top: 50px;
		}

		.columna{
			width: 30%;
			display: inline-block;
			height: 75px;
			margin: 10px;
			margin-left: 0;
		}

		.divisor{
			margin:50px 15px;
			border: 0.5px solid #808390;

		}

		.consideracion-texto{
			color: #26414b;
			font-weight: bold;
			font-size: 24px;
			margin-bottom: -12px;
		}

		.consideracion-descripcion{
			font-size: 20px;
			color: #26414b;
			font-weight: lighter;
		}

		.precio{
			display: inline-block;
			margin-top: -10px;
		}

		.precio-cantidad{
			float: right;
			margin-left: 20px;
			color: #264B41;
			font-size: 20px;
		}

		.precio-texto{
			float: left;
			font-weight: bold;
			color: #808390;
			font-size: 20px;
		}

		#asesor{
			margin: 100px 0;
		}

		#asesor .linea{
			border: 0.5px solid #808390;
			width: 250px;
			margin-left: 20px;
		}

		#asesor .firma{
			margin-left: 70px;
			font-size: 20px;
			font-weight: bold;
			color: #808390;
		}

		#pagina{
			display: inline-block;
			float: right;
		}

		#pagina p{
			text-align: right;
			color: #808390;
			font-weight: bold;
			font-size: 24px;
		}

	</style>
</head>
<body>

<div id="encabezado">
	<div class="texto">
		<p>Extranet</p>
	</div>
	<div class="imagen">
		<img src="volkswagen-logo.png" alt="">
	</div>
</div>
<div id="consecionario">
	<div class="consecionario-texto">
		<p class="nombre-consecionario">Consecionario VW</p>
		<p class="numero">Cotización #{{ $orden->id }}</p>
		<p class="cliente input">Cliente</p>
		<p class="nombre-cliente input-informacion">{{ $orden->contacto_nombres . ' ' . $orden->contacto_apellidos }}</p>
	</div>
	<div class="consecionario-imagen">
		<img src="tiguan.png" alt="" >
	</div>
</div>

<div class="informacion">
	<div class="columna">
		<p class="cliente input">De</p>
		<p class="nombre-cliente input-informacion">{{ $orden->vendedor_nombres . ' ' . $orden->vendedor_apellidos }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Teléfonos</p>
		<p class="nombre-cliente input-informacion">{{ $orden->vendedor_telefono }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Celular</p>
		<p class="nombre-cliente input-informacion"></p>
	</div>
	<div class="columna">
		<p class="cliente input">Email</p>
		<p class="nombre-cliente input-informacion">{{ $orden->vendedor_email }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Fecha</p>
		<p class="nombre-cliente input-informacion">{{ $orden->created_at }}</p>
	</div>
</div>

<div class="divisor">
</div>

<div class="consideracion">
	<p class="consideracion-texto">De nuestra consideración</p>
	<p class="consideracion-descripcion">Por medio de la presente hacemos llegar nuestra mejor oferta para el modelo Vollswagen solicitado</p>
</div>

<div class="informacion">
	<div class="columna">
		<p class="cliente input">Marca</p>
		<p class="nombre-cliente input-informacion">Volskwagen</p>
	</div>
	<div class="columna">
		<p class="cliente input">Modelo</p>
		<p class="nombre-cliente input-informacion">{{ App\Modelo::find($orden->modelo_id)->nombre }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Versión</p>
		<p class="nombre-cliente input-informacion">{{ App\Version::find($orden->version_id)->nombre }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Color</p>
		<p class="nombre-cliente input-informacion">{{ $orden->color }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Combustible</p>
		<p class="nombre-cliente input-informacion">{{ $orden->combusgtible }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Código de modelo</p>
		<p class="nombre-cliente input-informacion">{{ $orden->modelo_id }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Año de fabricación</p>
		<p class="nombre-cliente input-informacion">{{ $orden->fabricacion }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Año de modelo</p>
		<p class="nombre-cliente input-informacion">{{ $orden->anio }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Cantidad</p>
		<p class="nombre-cliente input-informacion">{{ $orden->cantidad }}</p>
	</div>
</div>

<div class="divisor">
</div>

<div class="consideracion">
	<p class="consideracion-texto">De nuestra consideración</p>
</div>

<div class="informacion">
	<div class="columna">
		<p class="cliente input">Precio de lista</p>
		<p class="nombre-cliente input-informacion">S/. {{ $orden->precio_lista_soles }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Bono</p>
		<p class="nombre-cliente input-informacion">S/. {{ $orden->bono_soles }}</p>
	</div>
	<div class="columna">
		<p class="cliente input">Precio de oferta</p>
		<p class="nombre-cliente input-informacion">S/. {{ $orden->precio_sugerido_soles}}</p>
	</div>
</div>

<div class="informacion">
	@foreach(App\Aditional::where('orden_id', $orden->id)->get() as $key => $aditional)
		<div class="columna">
			<p class="cliente input">Producto {{ $key + 1 }}</p>
			<p class="nombre-cliente input-informacion">{{ $aditional->producto }}</p>
			<div class="precio">
				<p class="precio-texto">Precio</p>
				<p class="precio-cantidad">S/. {{ $aditional->precio }}</p>
			</div>
		</div>
	@endforeach
</div>

<div id="asesor">
	<div class="linea"></div>
	<p class="firma">Asesor de ventas</p>
</div>

<div id="pagina">
	<p>Paginá 1</p>
</div>


</body>
</html>