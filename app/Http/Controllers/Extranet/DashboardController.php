<?php

namespace App\Http\Controllers\Extranet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

  public function main(Request $request)
  {
    return view('extranet.dashboard.main');
  }


}