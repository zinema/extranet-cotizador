
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: extranet_db
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aditionals`
--

DROP TABLE IF EXISTS `aditionals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aditionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `orden_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aditionals`
--

LOCK TABLES `aditionals` WRITE;
/*!40000 ALTER TABLE `aditionals` DISABLE KEYS */;
INSERT INTO `aditionals` VALUES (1,'AA111',1213,58,'2017-11-06 22:40:15','2017-11-06 22:40:15'),(2,'BB222',12312312,58,'2017-11-06 22:40:15','2017-11-06 22:40:15'),(3,'CC333',124,58,'2017-11-06 22:40:15','2017-11-06 22:40:15'),(4,'DD444',4234,58,'2017-11-06 22:40:15','2017-11-06 22:40:15');
/*!40000 ALTER TABLE `aditionals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2017-10-08 00:55:36','2017-10-08 00:55:36'),(2,NULL,1,'Category 2','category-2','2017-10-08 00:55:36','2017-10-08 00:55:36');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dni` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fuente_contacto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urgencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orden_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'98423212','Jesus','Carbajal','989843123','antoncarbaro@gmail.com','92385293',NULL,47,'2017-10-22 16:37:25','2017-10-22 16:37:25'),(2,'dasda','asdas','dasdas','sdasd','asd',NULL,NULL,48,'2017-10-22 17:03:22','2017-10-22 17:03:22'),(3,'123123','sadasd','asdasd','123123','asdasd','123123',NULL,49,'2017-10-22 17:05:25','2017-10-22 17:05:25'),(4,'12312','asdasd','asdasd','312312','asdasd',NULL,NULL,50,'2017-10-22 17:05:52','2017-10-22 17:05:52'),(5,'123123','asdasd','asdasdasd','123123','asdasd',NULL,NULL,51,'2017-10-22 17:06:18','2017-10-22 17:06:18'),(6,'1231','asdasd','asdasd','23123','asdasd',NULL,NULL,52,'2017-10-22 17:34:40','2017-10-22 17:34:40'),(7,'asda','asda','sdasd','sdasd','asd',NULL,NULL,53,'2017-10-22 17:42:30','2017-10-22 17:42:30'),(8,'sdasd','asdas','dasda','asda','sdasd','asd',NULL,54,'2017-10-22 17:43:17','2017-10-22 17:43:17'),(9,'dasd','zX','asdas','asdasd','asdasd','asdasd',NULL,55,'2017-10-22 17:53:28','2017-10-22 17:53:28'),(10,'asdas','asdasd','asdasd','dasd','asdasd',NULL,NULL,56,'2017-10-22 17:54:30','2017-10-22 17:54:30'),(11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,57,'2017-10-22 17:55:03','2017-10-22 17:55:03'),(12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:55:51','2017-10-22 17:55:51'),(13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:56:06','2017-10-22 17:56:06'),(14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:56:19','2017-10-22 17:56:19'),(15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 18:07:44','2017-10-22 18:07:44'),(16,'92239293','Anthony','Carbajal','91281291','antoncarbaro@gmail.com','9293923',NULL,58,'2017-11-06 22:39:38','2017-11-06 22:39:38');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'',1),(2,1,'author_id','text','Author',1,0,1,1,0,1,'',2),(3,1,'category_id','text','Category',1,0,1,1,1,0,'',3),(4,1,'title','text','Title',1,1,1,1,1,1,'',4),(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',5),(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),(7,1,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(8,1,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',8),(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,'',9),(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,'',10),(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,'',12),(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',13),(14,2,'id','number','id',1,0,0,0,0,0,'',1),(15,2,'author_id','text','author_id',1,0,0,0,0,0,'',2),(16,2,'title','text','title',1,1,1,1,1,1,'',3),(17,2,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',4),(18,2,'body','rich_text_box','body',1,0,1,1,1,1,'',5),(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),(20,2,'meta_description','text','meta_description',1,0,1,1,1,1,'',7),(21,2,'meta_keywords','text','meta_keywords',1,0,1,1,1,1,'',8),(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(23,2,'created_at','timestamp','created_at',1,1,1,0,0,0,'',10),(24,2,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',11),(25,2,'image','image','image',0,1,1,1,1,1,'',12),(26,3,'id','number','id',1,0,0,0,0,0,'',1),(27,3,'name','text','name',1,1,1,1,1,1,'',2),(28,3,'email','text','email',1,1,1,1,1,1,'',3),(29,3,'password','password','password',0,0,0,1,1,0,'',4),(30,3,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(31,3,'remember_token','text','remember_token',0,0,0,0,0,0,'',5),(32,3,'created_at','timestamp','created_at',0,1,1,0,0,0,'',6),(33,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(34,3,'avatar','image','avatar',0,1,1,1,1,1,'',8),(35,5,'id','number','id',1,0,0,0,0,0,'',1),(36,5,'name','text','name',1,1,1,1,1,1,'',2),(37,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(38,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(39,4,'id','number','id',1,0,0,0,0,0,'',1),(40,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(41,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),(42,4,'name','text','name',1,1,1,1,1,1,'',4),(43,4,'slug','text','slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(44,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),(45,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(46,6,'id','number','id',1,0,0,0,0,0,'',1),(47,6,'name','text','Name',1,1,1,1,1,1,'',2),(48,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(49,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(50,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(51,1,'seo_title','text','seo_title',0,1,1,1,1,1,'',14),(52,1,'featured','checkbox','featured',1,1,1,1,1,1,'',15),(53,3,'role_id','text','role_id',1,1,1,1,1,1,'',9),(54,7,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(55,7,'vendedor_nombres','text','Vendedor Nombres',0,1,1,1,1,1,NULL,2),(56,7,'vendedor_apellidos','text','Vendedor Apellidos',0,1,1,1,1,1,NULL,3),(57,7,'vendedor_email','text','Vendedor Email',0,1,1,1,1,1,NULL,4),(58,7,'vendedor_telefono','text','Vendedor Telefono',0,1,1,1,1,1,NULL,5),(59,7,'empresa_razon_social','text','Empresa Razon Social',0,1,1,1,1,1,NULL,6),(60,7,'empresa_direccion','text','Empresa Direccion',0,1,1,1,1,1,NULL,7),(61,7,'empresa_ruc','text','Empresa Ruc',0,1,1,1,1,1,NULL,8),(62,7,'empresa_telefono','text','Empresa Telefono',0,1,1,1,1,1,NULL,9),(63,7,'contacto_nombres','text','Contacto Nombres',0,1,1,1,1,1,NULL,10),(64,7,'contacto_apellidos','text','Contacto Apellidos',0,1,1,1,1,1,NULL,11),(65,7,'contacto_celular','text','Contacto Celular',0,1,1,1,1,1,NULL,12),(66,7,'contacto_telefono','text','Contacto Telefono',0,1,1,1,1,1,NULL,13),(67,7,'contacto_email','text','Contacto Email',0,1,1,1,1,1,NULL,14),(68,7,'contacto_fuente','text','Contacto Fuente',0,1,1,1,1,1,NULL,15),(69,7,'contacto_fecha_compra','timestamp','Contacto Fecha Compra',0,1,1,1,1,1,NULL,16),(70,7,'modelo_id','text','Modelo Id',0,1,1,1,1,1,NULL,17),(71,7,'version_id','text','Version Id',0,1,1,1,1,1,NULL,18),(72,7,'cantidad','text','Cantidad',0,1,1,1,1,1,NULL,19),(73,7,'color','text','Color',0,1,1,1,1,1,NULL,20),(74,7,'fabricacion','text','Fabricacion',0,1,1,1,1,1,NULL,21),(75,7,'anio','text','Anio',0,1,1,1,1,1,NULL,22),(76,7,'combustible','text','Combustible',0,1,1,1,1,1,NULL,23),(77,7,'deleted_at','timestamp','Deleted At',0,1,1,1,1,1,NULL,24),(78,7,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,25),(79,7,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,26),(80,7,'precio_lista_dolares','text','Precio Lista Dolares',0,1,1,1,1,1,NULL,27),(81,7,'bono_dolares','text','Bono Dolares',0,1,1,1,1,1,NULL,28),(82,7,'precio_sugerido_dolares','text','Precio Sugerido Dolares',0,1,1,1,1,1,NULL,29),(83,7,'precio_lista_soles','text','Precio Lista Soles',0,1,1,1,1,1,NULL,30),(84,7,'bono_soles','text','Bono Soles',0,1,1,1,1,1,NULL,31),(85,7,'precio_segurido_soles','text','Precio Segurido Soles',0,1,1,1,1,1,NULL,32),(86,7,'flete','text','Flete',0,1,1,1,1,1,NULL,33),(87,7,'validez_oferta','text','Validez Oferta',0,1,1,1,1,1,NULL,34),(88,7,'pdf','hidden','Pdf',0,1,1,1,1,1,NULL,35),(89,10,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(90,10,'dni','text','Dni',0,1,1,1,1,1,NULL,2),(91,10,'nombres','text','Nombres',0,1,1,1,1,1,NULL,3),(92,10,'apellidos','text','Apellidos',0,1,1,1,1,1,NULL,4),(93,10,'telefono','text','Telefono',0,1,1,1,1,1,NULL,5),(94,10,'email','text','Email',0,1,1,1,1,1,NULL,6),(95,10,'fuente_contacto','text','Fuente Contacto',0,1,1,1,1,1,NULL,7),(96,10,'urgencia','checkbox','Urgencia',0,1,1,1,1,1,NULL,8),(97,10,'orden_id','checkbox','Orden Id',0,1,1,1,1,1,NULL,9),(98,10,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,10),(99,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,11),(100,7,'dni_cliente','text','Dni Cliente',0,1,1,1,1,1,NULL,36),(194,25,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(195,25,'nombre','checkbox','Nombre',1,1,1,1,1,1,NULL,2),(196,25,'activo','checkbox','Activo',1,1,1,1,1,1,NULL,3),(197,25,'seo','checkbox','Seo',1,1,1,1,1,1,NULL,4),(198,25,'precio_soles','checkbox','Precio Soles',1,1,1,1,1,1,NULL,5),(199,25,'precio_dolares','checkbox','Precio Dolares',1,1,1,1,1,1,NULL,6),(200,25,'precio_integer','checkbox','Precio Integer',1,1,1,1,1,1,NULL,7),(201,25,'precio_string','checkbox','Precio String',1,1,1,1,1,1,NULL,8),(202,25,'bono_soles','checkbox','Bono Soles',1,1,1,1,1,1,NULL,9),(203,25,'bono_dolares','checkbox','Bono Dolares',1,1,1,1,1,1,NULL,10),(204,25,'velocidades','checkbox','Velocidades',1,1,1,1,1,1,NULL,11),(205,25,'image_big','checkbox','Image Big',1,1,1,1,1,1,NULL,12),(206,25,'image_media','checkbox','Image Media',1,1,1,1,1,1,NULL,13),(207,25,'nombre_imagen','checkbox','Nombre Imagen',1,1,1,1,1,1,NULL,14),(208,25,'cantidad_imagen','checkbox','Cantidad Imagen',1,1,1,1,1,1,NULL,15),(209,25,'descripcion','checkbox','Descripcion',1,1,1,1,1,1,NULL,16),(210,25,'pdf_nombre','checkbox','Pdf Nombre',1,1,1,1,1,1,NULL,17),(211,25,'deleted_at','timestamp','Deleted At',0,1,1,1,1,1,NULL,18),(212,25,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,19),(213,25,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,20),(214,25,'categoria_id','checkbox','Categoria Id',1,1,1,1,1,1,NULL,21),(215,25,'tipo_id','checkbox','Tipo Id',1,1,1,1,1,1,NULL,22),(216,25,'imagen_1','checkbox','Imagen 1',0,1,1,1,1,1,NULL,23),(217,25,'imagen_2','checkbox','Imagen 2',0,1,1,1,1,1,NULL,24),(218,25,'imagen_3','checkbox','Imagen 3',0,1,1,1,1,1,NULL,25),(219,25,'imagen_4','checkbox','Imagen 4',0,1,1,1,1,1,NULL,26),(220,25,'imagen_5','checkbox','Imagen 5',0,1,1,1,1,1,NULL,27),(221,25,'img_1','checkbox','Img 1',0,1,1,1,1,1,NULL,28),(222,25,'img_2','checkbox','Img 2',0,1,1,1,1,1,NULL,29),(223,25,'img_3','checkbox','Img 3',0,1,1,1,1,1,NULL,30),(224,25,'img_4','checkbox','Img 4',0,1,1,1,1,1,NULL,31),(225,25,'img_5','checkbox','Img 5',0,1,1,1,1,1,NULL,32),(226,25,'imagen_principal_desktop','checkbox','Imagen Principal Desktop',0,1,1,1,1,1,NULL,33),(227,25,'imagen_principal_mobile','checkbox','Imagen Principal Mobile',0,1,1,1,1,1,NULL,34),(228,26,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(229,26,'nombre','checkbox','Nombre',1,1,1,1,1,1,NULL,2),(230,26,'activo','checkbox','Activo',1,1,1,1,1,1,NULL,3),(231,26,'seo','checkbox','Seo',1,1,1,1,1,1,NULL,4),(232,26,'precio_soles','checkbox','Precio Soles',1,1,1,1,1,1,NULL,5),(233,26,'precio_dolares','checkbox','Precio Dolares',1,1,1,1,1,1,NULL,6),(234,26,'precio_integer','checkbox','Precio Integer',1,1,1,1,1,1,NULL,7),(235,26,'precio_string','checkbox','Precio String',1,1,1,1,1,1,NULL,8),(236,26,'descripcion','checkbox','Descripcion',1,1,1,1,1,1,NULL,9),(237,26,'cilindrada','checkbox','Cilindrada',1,1,1,1,1,1,NULL,10),(238,26,'transmision','checkbox','Transmision',1,1,1,1,1,1,NULL,11),(239,26,'velocidades','checkbox','Velocidades',1,1,1,1,1,1,NULL,12),(240,26,'potencia','checkbox','Potencia',1,1,1,1,1,1,NULL,13),(241,26,'par_neto','checkbox','Par Neto',1,1,1,1,1,1,NULL,14),(242,26,'deleted_at','timestamp','Deleted At',0,1,1,1,1,1,NULL,15),(243,26,'created_at','timestamp','Created At',1,1,1,1,0,1,NULL,16),(244,26,'updated_at','timestamp','Updated At',1,0,0,0,0,0,NULL,17),(245,26,'modelo_id','checkbox','Modelo Id',1,1,1,1,1,1,NULL,18),(246,26,'version_belongsto_model_relationship','relationship','models',0,1,1,1,1,1,'{\"model\":\"App\\\\Modelo\",\"table\":\"models\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":null,\"label\":null,\"pivot_table\":\"categories\",\"pivot\":\"0\"}',19),(247,27,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(248,27,'producto','text','Producto',0,1,1,1,1,1,NULL,2),(249,27,'precio','text','Precio',0,1,1,1,1,1,NULL,3),(250,27,'orden_id','checkbox','Orden Id',0,1,1,1,1,1,NULL,4),(251,27,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,5),(252,27,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(253,28,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(254,28,'saler_id','select_dropdown','Saler Id',0,1,1,1,1,1,'{\"default\":\"option1\",\"options\":{\"option1\":\"Option 1 Text\",\"option2\":\"Option 2 Text\"}}',2),(255,28,'user_id','select_multiple','User Id',0,1,1,1,1,1,NULL,3),(256,28,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),(257,28,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,'2017-10-08 00:55:31','2017-10-08 00:55:31'),(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,'2017-10-08 00:55:31','2017-10-08 00:55:31'),(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,'2017-10-08 00:55:32','2017-10-08 00:55:32'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,'2017-10-08 00:55:32','2017-10-08 00:55:32'),(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,'2017-10-08 00:55:32','2017-10-08 00:55:32'),(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,'2017-10-08 00:55:32','2017-10-08 00:55:32'),(7,'ordenes','ordenes','Orden','Ordenes',NULL,'App\\Orden',NULL,NULL,NULL,1,0,'2017-10-19 18:29:17','2017-10-19 18:29:17'),(10,'clients','clients','Cliente','Clients',NULL,'App\\Cliente',NULL,NULL,NULL,1,0,'2017-10-22 09:41:33','2017-10-22 09:41:33'),(25,'models','models','Modelo','Models',NULL,'App\\Modelo',NULL,NULL,NULL,1,0,'2017-10-22 16:26:54','2017-10-22 16:26:54'),(26,'versions','versions','Version','Versions',NULL,'App\\Version',NULL,NULL,NULL,1,0,'2017-10-22 16:27:06','2017-10-22 16:27:06'),(27,'aditionals','aditionals','Aditional','Aditionals',NULL,'App\\Aditional',NULL,NULL,NULL,1,0,'2017-11-01 03:14:47','2017-11-01 03:14:47'),(28,'saler_users','saler-users','Saler User','Saler Users',NULL,'App\\SalerUser',NULL,NULL,NULL,1,0,'2017-11-10 02:27:32','2017-11-10 02:27:32');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.media.index',NULL),(3,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.posts.index',NULL),(4,1,'Users','','_self','voyager-person',NULL,NULL,3,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.users.index',NULL),(5,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.categories.index',NULL),(6,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.pages.index',NULL),(7,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.roles.index',NULL),(8,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2017-10-08 00:55:33','2017-10-08 00:55:33',NULL,NULL),(9,1,'Menu Builder','','_self','voyager-list',NULL,8,10,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.menus.index',NULL),(10,1,'Database','','_self','voyager-data',NULL,8,11,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.database.index',NULL),(11,1,'Compass','/admin/compass','_self','voyager-compass',NULL,8,12,'2017-10-08 00:55:33','2017-10-08 00:55:33',NULL,NULL),(12,1,'Hooks','/admin/hooks','_self','voyager-hook',NULL,8,13,'2017-10-08 00:55:33','2017-10-08 00:55:33',NULL,NULL),(13,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2017-10-08 00:55:33','2017-10-08 00:55:33','voyager.settings.index',NULL),(14,1,'Aditionals','/admin/aditionals','_self',NULL,NULL,NULL,15,'2017-11-01 03:14:47','2017-11-01 03:14:47',NULL,NULL),(15,1,'Saler Users','/admin/saler-users','_self',NULL,NULL,NULL,16,'2017-11-10 02:27:32','2017-11-10 02:27:32',NULL,NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2017-10-08 00:55:33','2017-10-08 00:55:33');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_01_01_000000_create_pages_table',1),(6,'2016_01_01_000000_create_posts_table',1),(7,'2016_02_15_204651_create_categories_table',1),(8,'2016_05_19_173453_create_menu_table',1),(9,'2016_10_21_190000_create_roles_table',1),(10,'2016_10_21_190000_create_settings_table',1),(11,'2016_11_30_135954_create_permission_table',1),(12,'2016_11_30_141208_create_permission_role_table',1),(13,'2016_12_26_201236_data_types__add__server_side',1),(14,'2017_01_13_000000_add_route_to_menu_items_table',1),(15,'2017_01_14_005015_create_translations_table',1),(16,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),(17,'2017_01_15_000000_create_permission_groups_table',1),(18,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(19,'2017_03_06_000000_add_controller_to_data_types_table',1),(20,'2017_04_11_000000_alter_post_nullable_fields_table',1),(21,'2017_04_21_000000_add_order_to_data_rows_table',1),(22,'2017_07_05_210000_add_policyname_to_data_types_table',1),(23,'2017_08_05_000000_add_group_to_settings_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(2) NOT NULL DEFAULT '1',
  `seo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `precio_soles` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `precio_dolares` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `precio_integer` int(11) NOT NULL,
  `precio_string` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bono_soles` int(11) NOT NULL,
  `bono_dolares` int(11) NOT NULL,
  `velocidades` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_big` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_media` text COLLATE utf8_unicode_ci NOT NULL,
  `nombre_imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad_imagen` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `pdf_nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `imagen_1` text COLLATE utf8_unicode_ci,
  `imagen_2` text COLLATE utf8_unicode_ci,
  `imagen_3` text COLLATE utf8_unicode_ci,
  `imagen_4` text COLLATE utf8_unicode_ci,
  `imagen_5` text COLLATE utf8_unicode_ci,
  `img_1` text COLLATE utf8_unicode_ci,
  `img_2` text COLLATE utf8_unicode_ci,
  `img_3` text COLLATE utf8_unicode_ci,
  `img_4` text COLLATE utf8_unicode_ci,
  `img_5` text COLLATE utf8_unicode_ci,
  `imagen_principal_desktop` text COLLATE utf8_unicode_ci,
  `imagen_principal_mobile` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (1,'Up',1,'up','33,467','9,990',9990,'5',4355,1300,'5','Mecánico','modelos/July2017/cJmX4hvPA9NCUyHiHzsG.png','Auto Volkswagen Up Hatchback Vista Modelo','5','El up! es el más pequeño de la familia y sin embargo, cuenta con lo último en muchos aspectos. Su diseño moderno y compacto destaca por sus líneas horizontales en el frontal que le confieren un carácter diferente e innovador.','Up.pdf',NULL,'2017-06-04 05:00:00','2017-10-20 12:33:53',2,1,'modelos/October2017/y4E8MuZgJ5yXrb01HaAV.png','modelos/October2017/JngTORFttWoNAmzxFeaB.jpg','modelos/October2017/c7KmeLCALmo2QSNjAUq5.jpg','modelos/October2017/tGq3bjwyIY2MEK7moDjF.jpg','modelos/October2017/OET0LMBVTugrfhoCMGfz.jpg','modelos/October2017/6CBkGuEz31wxhz5dpPn6.png','modelos/October2017/YgdGSUqQP5vWOXzObfEs.jpg','modelos/October2017/zK6JiTOONJOg24elrfnQ.jpg','modelos/October2017/HiyvGastF8ZrMX7nLKGp.jpg','modelos/October2017/KqxyLe3wpxTyw5uRDcvr.jpg','modelos/October2017/SHg801YEOutM7rlr1Voi.png','modelos/October2017/Gm7nyRhTQ8okKCG2plfS.png'),(2,'Gol Hatchback',1,'gol-hatchback','38,492','11,490',11490,'5',5025,1500,'5','Mecánico/Automátizado','modelos/July2017/FHvMiwSu3PewR5T115YC.png','Auto Volkswagen Gol Hatchback Vista Modelo','6','Ahora podrás adueñarte de las calles: el Gol será el auto que te lleve a donde quieras. Juntos formarán un equipo que estará listo para ganar. Ahora te toca decidir: ¿Cuál es tu estilo de juego?.','Gol.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 16:52:59',2,3,'modelos/October2017/yYtI0ntCPAbpgZlqBtKx.jpg','modelos/October2017/777idj69ZUu9g03DXBMd.jpg','modelos/October2017/5QrvzwmX12NIaVgzP4J9.jpg','modelos/October2017/8PhFVQFHEQ8eOeKt1YmV.jpg','modelos/October2017/LXSlV09uvvs61SiESdG8.jpg','modelos/October2017/aJQU2aYguROCL9rpgPGC.jpg','modelos/October2017/C0ix9jZyJnbf8cFW5Oj4.jpg','modelos/October2017/bZAjnl19nuYxIczhTFev.jpg','modelos/October2017/bd8x7ZPGG8mRWc8HLYf4.jpg','modelos/October2017/AlQ4KuN9IBr5eeIdmk4S.jpg','modelos/October2017/0IbBwwDP48qsmRU6Yxsu.jpg','modelos/October2017/uapMVY8GLfCZxa3Ce7Pj.jpg'),(3,'Gol Sedan',1,'gol-sedan','36,147','10,790',10790,'5',4690,1400,'5','Mecánico/Automatizado','modelos/July2017/1BLJIIoXLs4U6WfP3c2u.png','Auto Volkswagen Gol Sedan Vista Modelo','6','Ahora mucho más moderno y versátil, tu nuevo Gol ahora te permite mantenerte conectado en todo momento, no te vas a perder de nada con un auto que de tan solo verlo nos inspira el inevitable deseo de manejar.','Gol Sedán.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 16:42:00',1,2,'modelos/October2017/nlaKpAXcG7cJGbXlDRX7.jpg','modelos/October2017/pmvZgjg5ppp8f18PcsOH.jpg','modelos/October2017/thrtr2uN2hOsTj1U7acP.jpg','modelos/October2017/8RPOvJ4rJ0xJqeFBqURy.jpg','modelos/October2017/j2qkAgtqGCDMIehKY9ZI.jpg','modelos/October2017/Ww9PNOpxxpm1yAt9CvnX.jpg','modelos/October2017/0q4SpxVoBd1ZrrhL9o2q.jpg','modelos/October2017/goKRV4Rkk7ok4bGbIgDw.jpg','modelos/October2017/nGjfNM1EqEHvV1QE1cUY.jpg','modelos/October2017/1XBYWXIsASubHbpQWniW.jpg','modelos/October2017/yCpb1K9NxfXQNpddPkfK.jpg','modelos/October2017/QXA6tAUl1brsbnkzXHuo.jpg'),(4,'Golf',1,'golf','63,617','18,990',18990,'5/6/7',10050,3000,'5/6/7','Mecánico/Automático','modelos/July2017/GNLSnWZ4dSagkP9J1q2c.png','Auto Volkswagen Golf Hatchback Vista Modelo','6','El nuevo Golf crea una nueva categoría de diseño. Ahora es más impactante, deportivo y aerodinámico, pero sigue manteniéndose fiel a sí mismo. Tan nuevo como intemporalmente elegante.','Golf.pdf',NULL,'2017-06-04 05:00:00','2017-10-20 13:53:48',2,3,'modelos/October2017/FLFUXPMnhrJkGKow3Dm8.png','modelos/October2017/M7WnB0oOUJaGTe76qZUG.png','modelos/October2017/wjSuhAKsR5p7EsP9mnFi.png','modelos/October2017/2B4KLH1nTExuSCC65TFx.png','modelos/October2017/rE6E1ROrbgmh3uD6URjG.png','modelos/October2017/i8FMF42Yi2GP9OlSXJq6.png','modelos/October2017/axhoGaLUcGZEN1Rww8Q0.png','modelos/October2017/eGUeHE0QAUCNqQ8xgKJH.png','modelos/October2017/bQNgXQDd9t0aBXzJuohB.png','modelos/October2017/u34GQhN4s2FAUevqa2Cf.png','modelos/October2017/X7mE24VE8rF2BQi3pbNY.png','modelos/October2017/cSNuYLAYpmUl57spa6wO.png'),(5,'Golf GTI',1,'golf-gti','107,167','31,990',31990,'6',11725,3500,'6','Automático','modelos/July2017/shyl8JY7B2xpm9myk4Yl.png','Auto Volkswagen Golf GTI Hatchback Vista Modelo','6','Fiel a su carácter deportivo hasta en el más mínimo detalle, su espíritu sigue siendo el mismo. Aunque después de millones de kilómetros, curvas imposibles y rectas infinitas en algo sí ha cambiado. Ahora es más GTI, gracias al incremento de su potencia hasta los 220 CV de serie.','Golf GTI.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:52:46',2,3,'modelos/October2017/EeS3Ru5YnPXAWJNmTdQD.jpg','modelos/October2017/BkOAzdat7uJhp3igc0i9.jpg','modelos/October2017/K4WzQo85LeElYAIswgmy.jpg','modelos/October2017/eKZ2Sm2FtE2MOn77CxVv.jpg','modelos/October2017/osJTRcMMcl9CdSfYnTmA.jpg','modelos/October2017/Yq3KzcRcNwUDPfFTKEev.jpg','modelos/October2017/UyIVCY3iQIm0ZxjA4eCK.jpg','modelos/October2017/9R4RvJDb3FSp6u69DXJQ.jpg','modelos/October2017/WyjVd4zBOaBnGtcdWBu2.jpg','modelos/October2017/cR3P1EmEmMUiDOLXogqr.jpg','modelos/October2017/ke4xawnmHAu3jCEwqrXk.jpg','modelos/October2017/OpAByZQ57jQ37NPuGErh.jpg'),(6,'Jetta',1,'jetta','56,917','16,990',16990,'5/6 ',10050,3000,'5/6','Mecánico / Automático','modelos/July2017/4sVLTSfsZ5IyCsbSPaOR.png','Auto Volkswagen Jetta Sedan Vista Modelo','6','El Jetta sigue teniendo sus formas clásicas de berlina con zaga escalonada. Sus logradas\r\n\r\nproporciones y los detalles de su interior y exterior transmiten una elegancia y alcurnia atemporal.','Jetta.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:10:01',1,1,'modelos/October2017/ci1cTvUzABIMBSTInQFE.jpg','modelos/October2017/1IxWUgKU7NFwpGsN7i0Y.jpg','modelos/October2017/A64xgDVKxfXMbOOST21p.jpg','modelos/October2017/jnRfqLosRBfAJ8QK5sqP.jpg','modelos/October2017/l4Lkcp2C1O2DW2VjzKz9.jpg','modelos/October2017/Xl0JPWfrrHcx9MjE6R9R.jpg','modelos/October2017/zCGotWPobLtf84TgGUHd.jpg','modelos/October2017/c59LvUUuErt1O8NZJd8q.jpg','modelos/October2017/Ylcin8bfXe0XUAog1V77.jpg','modelos/October2017/Ms4tSxL0FY4g0PuRROo8.jpg','modelos/October2017/PPeb3T8JJM5X7qWycxtG.jpg','modelos/October2017/g14ECn3TIOTXSSyd96cr.jpg'),(7,'Jetta GLI',1,'jetta-gli','90,417','26,990',26990,'6',15075,4500,'6','Mecánico / Automático','modelos/July2017/W4KjS2tA72ts2Y2u8PLS.png','Auto Volkswagen Jetta GLI Sedan Vista Modelo','3','Toda la elegancia de un Jetta convertida en un deportivo excepcional. Gracias a su motor 2.0 Turbo no dejarás rastro, pero sí huella en todos los que te vean con este auto de alta gama.','Jetta GLI.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:35:18',1,3,'modelos/October2017/3wB2LuToqOdUZ6QG925e.jpg','modelos/October2017/HgwMsnOoWkcCPWd95fR1.jpg','modelos/October2017/g8jsDNeQ4T98ENeWD06G.jpg',NULL,NULL,'modelos/October2017/b9PyUKgTrlKLKU0iQl2H.jpg','modelos/October2017/WO7w1fMytJWMr2QKzxGu.jpg','modelos/October2017/xle34SeFbvYbOLA1Wy8t.jpg',NULL,NULL,'modelos/October2017/Ka99cVOp9N0kXIbCAj4m.jpg','modelos/October2017/zwMmNG6rwGhjDeww0afG.jpg'),(8,'Crossfox',1,'crossfox','53,265','15,900 ',15900,'6/5',7035,2100,'6/5','Mecánico / Automático','modelos/July2017/JYEDTc5bmmOxwNBgdd4d.png','Auto Volkswagen Crossfox Hatchback Vista Modelo','4','El Crossfox te invita a comenzar tu próxima aventura. Su diseño deportivo y aventurero te cautiva desde el primer segundo. La parrilla de diseño horizontal y logo Crossfox manifiestan el lado más arriesgado de este gran miembro de la familia Volkswagen.','Crossfox.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 16:58:20',2,5,'modelos/October2017/DbeHFgBPZ2wbwPlif8pM.jpg','modelos/October2017/kZSbyEzND2MZEt9iGdy6.jpg','modelos/October2017/XLtJ0YkYrCK3hlolrohA.jpg','modelos/October2017/VNlFDKybHAbVRZlviJhH.jpg','modelos/October2017/0Z3AxHa8rVx39Ie8be1n.jpg','modelos/October2017/RRS8y4AEfGR23LTfS28l.jpg','modelos/October2017/T8UtH6Fa8SlZvz2zBRpe.jpg','modelos/October2017/jU10MR9tpGYfdjpdKRqp.jpg','modelos/October2017/KmVSamHx0jkAwflyBcJZ.jpg','modelos/October2017/67vzZaRg3MoMjuRFwCeq.jpg','modelos/October2017/4dzGFgXKmFi1Lpoi837k.jpg','modelos/October2017/bjalJacfkXcBe93bh20j.jpg'),(9,'Polo',1,'polo-hatchback','55,912','16,690',16690,'5',8710,2600,'5','Mecánico','modelos/July2017/Tt19CJKatheOSWQpXBCB.png','Auto Volkswagen Polo Hatchback Vista Modelo','3','Entusiasmate una y otra vez, como el primer dia.','Polo.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:01:59',2,1,'modelos/October2017/7YpQojaWvOUQ54GBYbqT.jpg','modelos/October2017/OvDRaDEQ1jgrNaeFakjk.jpg','modelos/October2017/LQJrpXdJbrbRJ1B7abiL.jpg',NULL,NULL,'modelos/October2017/DnjeqmwtggNUP1UkGs1A.jpg','modelos/October2017/hfOHf1faJZsOz9QxECTu.jpg','modelos/October2017/Vq3MOA53JZRJZrnmL13S.jpg',NULL,NULL,'modelos/October2017/T6H5l3wC6sHYQuGQ9Ua4.jpg','modelos/October2017/1lk2itRs1q7FIz9e6qwM.jpg'),(10,'Beetle',1,'beetle','73,667','21,990',21990,'6',11725,3500,'6','Automático','modelos/July2017/5vSeFJbI9JEFOEvtvt4X.png','Auto Volkswagen Beetle Hatchback Vista Modelo','4','Muy pocos automóviles han fascinado tanto como el Escarabajo. Ahora, el icono se reinventa.','Beetle.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:29:23',2,3,'modelos/October2017/ZHGpDP11ISNIVuW06bhZ.jpg','modelos/October2017/1YLW7U883M07GmE3XBi0.jpg','modelos/October2017/cP0B8tbp87S7tx2htT2M.jpg','modelos/October2017/A2qVN1TS0UhvafmapIqm.jpg',NULL,'modelos/October2017/r50pzjgHoYxhuUM3jweK.jpg','modelos/October2017/WbbNKYBiY2VaUxPPLFZ9.jpg','modelos/October2017/ZyV8olbdvjwhYHtMCRqU.jpg','modelos/October2017/hT9ZnsfareKms4gSDiz5.jpg',NULL,'modelos/October2017/a050WENTwrMDGmGRboYZ.jpg','modelos/October2017/Uia0yaLc6kP1zWN4pFZO.jpg'),(11,'Tiguan Allspace',1,'tiguan_allspace','92,092','27,490',91092,'6',6700,2000,'6','Automático','modelos/June2017/VMLBkFjpM6qWtCFm4dwT.png','PW_Camioneta Volkswagen Tiguan Allspace Vista Modelo','3','La Nueva Tiguan Allspace ofrece un ambiente aún más espacioso con espacio para 7 personas. Además cuenta con un sistema de control de tracción 4MOTION con función ECO, dispondrás de cuatro perfiles de conducción perfectamente adaptados a la carretera, a caminos offroad y a las situaciones meteorológicas más diversas.','CC.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:47:58',3,2,'modelos/October2017/CXPWYu5eOyFAAAzbHaTG.jpg','modelos/October2017/tyESRF8oQ9p5olViPEgt.jpg','modelos/October2017/c6N6ZRetnOoJBxXYyz4o.jpg','modelos/October2017/vXkBOxpPLhSLPDdVwkVR.jpg','modelos/October2017/9XVCxOtWBfzNjoB6vxFp.jpg','modelos/October2017/iOjZ31XdkL32eLhqmC8k.jpg','modelos/October2017/NKWspOKJoXWkhdCQU9H6.jpg','modelos/October2017/XljvlZgbzGUc2VVH7HFb.jpg','modelos/October2017/4WFZrZ7LoWmXOi7IQHmo.jpg','modelos/October2017/f2IOC56mlrNsPMScll6g.jpg','modelos/October2017/ML4QW6h1ap2ciSMPjKWW.jpg','modelos/October2017/D0zKMo4Et2RyYuT0G1Ve.jpg'),(12,'Tiguan',0,'tiguan','110,517','32,990',32990,'6/7',10050,3000,'6/7','Mecánico / Automático','modelos/July2017/8Q8bhKhFm4Hag937LjVP.png','Camioneta Volkswagen Tiguan Vista Modelo','4','La Tiguan ofrece un ambiente espacioso, exclusivo y elegante equipado con diferentes aditamentos que hacen de tu viaje y el de toda tu familia un placer. Además cuenta con un conjunto de sistemas de seguridad que permiten una excelente conducción en cualquier terreno.','Tiguan.pdf',NULL,'2017-06-04 05:00:00','2017-10-12 17:54:36',3,2,'modelos/July2017/NsLScKrukVQbZaVh2D2T.jpg','modelos/July2017/NsLyBkYzfFMiEsEUkzQx.jpg','modelos/July2017/LWmHY4QSUIwoHuTyTe5l.jpg','modelos/July2017/2gUP0c0OHyG7oiF3T5k2.jpg',NULL,'modelos/July2017/v8Esyqvrx7KOrlVdiZT8.jpg','modelos/July2017/gLUq9sNf3y5RPV3vpXAw.jpg','modelos/July2017/IDUdnTaQJcePM2m4wxyn.jpg','modelos/July2017/Od4SdgXj8f6q26sT3KzG.jpg',NULL,'modelos/July2017/cajQ0GJ15ft3K9W5KOGy.png','modelos/July2017/JenVTlYvOqLraqyR2sU5.png'),(13,'Touareg',1,'touareg','217,717','64,990',64990,'8',16750,5000,'8','Automático','modelos/July2017/Cez4eyGAmB1JXwW24cij.png','Camioneta Volkswagen Touareg Vista Modelo','6','Una tracción superior dentro y fuera de la carretera gracias a su potente motor, un confort excepcional y un diseño elegante y refinado: con la Touareg dominarás prácticamente cualquier aventura. Y el día a día.','Touareg.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:59:52',3,2,'modelos/October2017/bP65naabLfTMBZLPJ84x.jpg','modelos/October2017/PAD7GoEJ4diWLzZU4KvB.jpg','modelos/October2017/HfIoHVeaGuZp7NQ5kCIm.jpg','modelos/October2017/iCbrorKs3srhoLcpKoVh.jpg','modelos/October2017/jchGioSTNiAL5DEBdCML.jpg','modelos/October2017/XRs29IiO8qDMNijE5RnV.jpg','modelos/October2017/VMY1eqptuhFrgTLKN2jl.jpg','modelos/October2017/0dAvj1zvdLnvFMstnvnv.jpg','modelos/October2017/9TiN5VJyd9K3uRMXFxDX.jpg','modelos/October2017/KbL0HJsd9l4iuRS5HltC.jpg','modelos/October2017/7L6au3xTVp4sXPgfjGwX.jpg','modelos/October2017/31PvM2ln0opZcgh4n1F1.jpg'),(14,'Amarok',1,'amarok','92,092','27,490',27490,'6/8',5025,1500,'6/8','Mecánico / Automático','modelos/July2017/ccb2mFmcHpB1dZmJPJ3X.png','Camioneta Volkswagen Amarok Pickup Vista Modelo','4','La vida está llena de desafíos, ¿Estas listo?','Amarok.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:41:48',4,5,'modelos/October2017/eTJzDBdcwq4Csvbx05Mx.jpg','modelos/October2017/3Byb0wZchk3JgBnzgyNg.jpg','modelos/October2017/lAhgHmIXJLX8l10eYvzA.jpg','modelos/October2017/Lv0ryOrCDaFbDYbXtzoF.jpg',NULL,'modelos/October2017/DHLlOhpIFDsymurAxEBb.jpg','modelos/October2017/PoP3TBSNWpLr5SPOyoAo.jpg','modelos/October2017/sk50hQTSyHmRreYHlpb6.jpg','modelos/October2017/LktcvrtqI0FNKjUyY25X.jpg','modelos/July2017/lxInqBIBBfSOLT4Rtcol.jpg','modelos/October2017/1X6oRvk8qC1XIyOigLjB.jpg','modelos/October2017/wNyxHBTZtvJtHz2oySAc.jpg'),(15,'Transporter',1,'transporter','97,117','28,990',28990,'5',5052,1500,'5','Mecánico','modelos/July2017/MDDw0QZlQO6s0DZjCbJo.png','Auto Volkswagen Transporter Vehiculos Comerciales Vista Modelo','1','Cada cliente tiene necesidades distintas y cada tarea requiere su propio enfoque. Apuesta por un vehículo que tenga respuesta para todo.','Transporter.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:49:57',4,4,'modelos/October2017/rm2Ma0khoOmFiECQZwyV.jpg','modelos/July2017/bq8eg7cCUFKX2MOFDksA.jpg',NULL,NULL,NULL,'modelos/October2017/DtpFTIyG7geWp3XyyQdm.jpg','modelos/July2017/Ur5dhN4xz6A4Z6qjDI0L.jpg',NULL,NULL,NULL,'modelos/October2017/OBR4Sv0tTR0CdGrfn0G3.jpg','modelos/October2017/BLDTcjRoEZIO7vpojJ7T.jpg'),(16,'Caddy',1,'caddy','58,592','17,490',17490,'5',2680,800,'5','Mecánico','modelos/July2017/JAxgN8Vnqf2H9jOmjryK.png','Auto Volkswagen Caddy Vehiculos Comerciales Vista Modelo','3','Alta calidad, uso ingenioso del espacio y una eficiencia que abre nuevas perspectivas: la Caddy es un vehículo que marca la diferencia y se desempeña a la perfección en todas las tareas.','Caddy 4.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:18:13',4,4,'modelos/October2017/5vOJ2O3cxEpyRmlYWSXe.jpg','modelos/October2017/GjOgzSztWQZplnadfN6L.jpg','modelos/October2017/HjjacccLdtBlisy6LfXC.jpg','modelos/July2017/GlCtLvPgTCQEfcD5DYJp.jpg',NULL,'modelos/October2017/8SrmN2xb64s1wFjE66hL.jpg','modelos/October2017/Y5vVH8fesEGT91doOKbr.jpg','modelos/October2017/NarURDc3yIng8gpTWmsP.jpg','modelos/July2017/DSIJt4OKxNlwCKnnMbud.jpg',NULL,'modelos/October2017/MEilKgkHX7eFCsqG59at.jpg','modelos/October2017/LArlJxZE54Vco7EvZ5TY.jpg'),(17,'Crafter',1,'crafter','162,442','48,490',48490,'6',8375,2500,'6','Mecánico','modelos/July2017/RxnzTEY3kGoElprabvhQ.png','Auto Volkswagen Crafter Vehiculos Comerciales Vista Modelo','5','El Crafter es tan completo, robusto y de tanta calidad que puede realizar los cometidos de trabajo más pesados así como trasladar cómodamente al grupo más exigente de personas.','Crafter.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:56:43',4,4,'modelos/October2017/jd70pICSh0tQMpqcdP7k.jpg','modelos/October2017/Z2rVWGvXXkFUufPchbl9.jpg','modelos/October2017/vcuqz5Jl4Idp360kamTs.jpg','modelos/October2017/apI2U7fm7oTizORlcSNH.jpg','modelos/October2017/106eRrZ9H97w49lKHM0U.jpg','modelos/October2017/s7CQsNHCJzllSqZ2oWty.jpg','modelos/October2017/XXyML8ey88dNnbMM4Kjg.jpg','modelos/October2017/NrVSuXrfbxHBBenGQNV9.jpg','modelos/October2017/QwtGFZ3kK7gco97EcO63.jpg','modelos/October2017/HYDPU8iUhTLkipYWjjtT.jpg','modelos/October2017/1hoCK74gHm8GWRH97Xby.jpg','modelos/October2017/BuHGhgfIL5H4nEMolOxv.jpg'),(19,'Polo Sedán',1,'polo-sedan','56,917','16,990',16990,'5',8710,2600,'5','Mecánico','modelos/July2017/8gFZSf6MXcNj0WRAfp94.png','Auto Volkswagen Polo Sedan Vista Modelo','6','Todo el carácter del Polo, ahora en una versión sedán.','Polo.pdf',NULL,'2017-06-04 05:00:00','2017-10-16 17:15:40',1,1,'modelos/October2017/ocgbBY89dPOUTRt1cfNn.jpg','modelos/October2017/FYemIMgIJgE0uWr0eHIC.jpg','modelos/October2017/HJHH0oELvDX8JAdbKsm6.jpg','modelos/October2017/6uJUv5Zsp8hqxSBMGJCA.jpg','modelos/October2017/RHoOPJ4VSoGCgg4DNFxh.jpg','modelos/October2017/lfF339j7rz0njknocitP.jpg','modelos/October2017/MhlSiGjStgt40eTBYNat.jpg','modelos/October2017/rwjjncnUsMa1oDQocWOz.jpg','modelos/October2017/JObrbpuf4PnkuZ1KyVnF.jpg','modelos/October2017/KC2JtIa1ok3vcMwheqeC.jpg','modelos/October2017/FxRieFrAQRQe8Gvo46gf.jpg','modelos/October2017/NKlbinBx9bcqZJqFtCmp.jpg'),(20,'Spacefox',0,'spacefox','65,292','19,490',19490,'5',8375,2500,'5','Mecánico / Automático','modelos/July2017/4QAJ7Q2ePjP4F6cYBUVK.png','Auto Volkswagen Spacefox Vista Modelo','3','La deportividad, el espacio y el confort se conjugan en el nuevo Spacefox, permitiendo disfrutar a sus pasajeros de experiencias inolvidables.','spacefox.pdf',NULL,'2017-06-04 05:00:00','2017-07-27 14:50:37',2,1,'modelos/July2017/eBrHI259lc81Rj0hSeVr.jpg','modelos/July2017/4JartPCSb4V1GpSnjGHM.jpg','modelos/July2017/CnFlEcAQSqeIpCcfAFdi.jpg',NULL,NULL,'modelos/July2017/AmAtl6JF2ZyDitMMWTsj.jpg','modelos/July2017/LMgoeHchb2gGLvyv5ZeH.jpg','modelos/July2017/eIHAOd0fw7ZpZfCK89S1.jpg',NULL,NULL,'modelos/July2017/pJsrWlqK4UMDBnpEWo8z.png','modelos/July2017/I87oGuAWKQnCrEv4vL3i.png');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordenes`
--

DROP TABLE IF EXISTS `ordenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordenes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendedor_nombres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendedor_apellidos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendedor_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendedor_telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empresa_razon_social` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empresa_direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empresa_ruc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empresa_telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_nombres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_apellidos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_celular` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_fuente` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_fecha_compra` timestamp NULL DEFAULT NULL,
  `modelo_id` int(11) DEFAULT NULL,
  `version_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fabricacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `combustible` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `precio_lista_dolares` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bono_dolares` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio_sugerido_dolares` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio_lista_soles` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bono_soles` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio_segurido_soles` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flete` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `validez_oferta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` mediumtext COLLATE utf8_unicode_ci,
  `dni_cliente` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordenes`
--

LOCK TABLES `ordenes` WRITE;
/*!40000 ALTER TABLE `ordenes` DISABLE KEYS */;
INSERT INTO `ordenes` VALUES (42,'Admin','Jesus','admin@admin.com','957207941',NULL,NULL,NULL,NULL,'Anthony',NULL,'982990174',NULL,'antoncarbaro@gmail.com','983982234',NULL,1,1,1,'azul','2015','2015','gas',NULL,NULL,NULL,'10000','1000','10000','30000','3000',NULL,'1','2 semanas',NULL,NULL,1),(43,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 08:00:59','2017-10-22 08:00:59',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'cotizacion_1508659259_42.pdf',NULL,1),(44,'Admin','asdasd','admin@admin.com','123123123',NULL,NULL,NULL,NULL,'asdasd',NULL,'123123',NULL,'asdasd','1123',NULL,1,1,123,'asd','123','123','123',NULL,NULL,'2017-10-22 08:17:48','12','wqe','asd','asd','asd',NULL,'1','2 semanas','cotizacion_1508660268_44.pdf',NULL,2),(45,'Admin','rojitas','admin@admin.com','123123',NULL,NULL,NULL,NULL,'asdasd',NULL,'asdasd',NULL,'sadas','dasdas',NULL,1,1,0,'sdasd','dasda','sdas','dasda',NULL,'2017-10-22 08:20:33',NULL,'123','123','123','123','123',NULL,'1','asd',NULL,NULL,1),(46,'Admin','12123','admin@admin.com','123123',NULL,NULL,NULL,NULL,'asdasd',NULL,'23123',NULL,'123123',NULL,NULL,1,1,123,'123','123','123','asd',NULL,'2017-10-22 08:48:32','2017-10-22 08:48:49','123123','123123','123123','3123123','12312312',NULL,'1',NULL,'cotizacion_1508662129_46.pdf',NULL,2),(47,'Admin','Rojas','admin@admin.com','99349534',NULL,NULL,NULL,NULL,'Jesus','Carbajal','989843123',NULL,'antoncarbaro@gmail.com','92385293',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 16:36:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'98423212',1),(48,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,'asdas','dasdas','sdasd',NULL,'asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:03:16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dasda',1),(49,'Admin','asdasd','admin@admin.com','123123123',NULL,NULL,NULL,NULL,'sadasd','asdasd','123123',NULL,'asdasd','123123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:05:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123123',2),(50,'Admin','asdas','admin@admin.com','123123',NULL,NULL,NULL,NULL,'asdasd','asdasd','312312',NULL,'asdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:05:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'12312',1),(51,'Admin','asdasdasd','admin@admin.com','123123',NULL,NULL,NULL,NULL,'asdasd','asdasdasd','123123',NULL,'asdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:06:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123123',1),(52,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,'asdasd','asdasd','23123',NULL,'asdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:34:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1231',4),(53,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,'asda','sdasd','sdasd',NULL,'asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:42:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'asda',2),(54,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,'asdas','dasda','asda',NULL,'sdasd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:43:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sdasd',4),(55,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,'zX','asdas','asdasd',NULL,'asdasd','asdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:47:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dasd',2),(56,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,'asdasd','asdasd','dasd',NULL,'asdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:54:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'asdas',4),(57,'Admin','asdasd','admin@admin.com','asdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-22 17:55:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(58,'Admin','Rojas','admin@admin.com','928929329',NULL,NULL,NULL,NULL,'Anthony','Carbajal','91281291',NULL,'antoncarbaro@gmail.com','9293923',NULL,14,0,4,'azul','2016','2016','gas',NULL,'2017-11-06 22:39:37','2017-11-06 22:41:14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'cotizacion_1509990074_58.pdf','92239293',1),(59,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `ordenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,1,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/October2017/Captura de pantalla (6).png','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2017-10-08 00:55:36','2017-10-08 01:42:08');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(2,'browse_database',NULL,'2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(3,'browse_media',NULL,'2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(4,'browse_compass',NULL,'2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(5,'browse_menus','menus','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(6,'read_menus','menus','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(7,'edit_menus','menus','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(8,'add_menus','menus','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(9,'delete_menus','menus','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(10,'browse_pages','pages','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(11,'read_pages','pages','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(12,'edit_pages','pages','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(13,'add_pages','pages','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(14,'delete_pages','pages','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(15,'browse_roles','roles','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(16,'read_roles','roles','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(17,'edit_roles','roles','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(18,'add_roles','roles','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(19,'delete_roles','roles','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(20,'browse_users','users','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(21,'read_users','users','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(22,'edit_users','users','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(23,'add_users','users','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(24,'delete_users','users','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(25,'browse_posts','posts','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(26,'read_posts','posts','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(27,'edit_posts','posts','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(28,'add_posts','posts','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(29,'delete_posts','posts','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(30,'browse_categories','categories','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(31,'read_categories','categories','2017-10-08 00:55:34','2017-10-08 00:55:34',NULL),(32,'edit_categories','categories','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(33,'add_categories','categories','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(34,'delete_categories','categories','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(35,'browse_settings','settings','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(36,'read_settings','settings','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(37,'edit_settings','settings','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(38,'add_settings','settings','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(39,'delete_settings','settings','2017-10-08 00:55:35','2017-10-08 00:55:35',NULL),(40,'browse_ordenes','ordenes','2017-10-19 18:29:17','2017-10-19 18:29:17',NULL),(41,'read_ordenes','ordenes','2017-10-19 18:29:17','2017-10-19 18:29:17',NULL),(42,'edit_ordenes','ordenes','2017-10-19 18:29:17','2017-10-19 18:29:17',NULL),(43,'add_ordenes','ordenes','2017-10-19 18:29:17','2017-10-19 18:29:17',NULL),(44,'delete_ordenes','ordenes','2017-10-19 18:29:17','2017-10-19 18:29:17',NULL),(45,'browse_clients','clients','2017-10-22 09:41:33','2017-10-22 09:41:33',NULL),(46,'read_clients','clients','2017-10-22 09:41:33','2017-10-22 09:41:33',NULL),(47,'edit_clients','clients','2017-10-22 09:41:33','2017-10-22 09:41:33',NULL),(48,'add_clients','clients','2017-10-22 09:41:33','2017-10-22 09:41:33',NULL),(49,'delete_clients','clients','2017-10-22 09:41:33','2017-10-22 09:41:33',NULL),(70,'browse_models','models','2017-10-22 16:26:54','2017-10-22 16:26:54',NULL),(71,'read_models','models','2017-10-22 16:26:54','2017-10-22 16:26:54',NULL),(72,'edit_models','models','2017-10-22 16:26:54','2017-10-22 16:26:54',NULL),(73,'add_models','models','2017-10-22 16:26:54','2017-10-22 16:26:54',NULL),(74,'delete_models','models','2017-10-22 16:26:54','2017-10-22 16:26:54',NULL),(75,'browse_versions','versions','2017-10-22 16:27:07','2017-10-22 16:27:07',NULL),(76,'read_versions','versions','2017-10-22 16:27:07','2017-10-22 16:27:07',NULL),(77,'edit_versions','versions','2017-10-22 16:27:07','2017-10-22 16:27:07',NULL),(78,'add_versions','versions','2017-10-22 16:27:07','2017-10-22 16:27:07',NULL),(79,'delete_versions','versions','2017-10-22 16:27:07','2017-10-22 16:27:07',NULL),(80,'browse_aditionals','aditionals','2017-11-01 03:14:47','2017-11-01 03:14:47',NULL),(81,'read_aditionals','aditionals','2017-11-01 03:14:47','2017-11-01 03:14:47',NULL),(82,'edit_aditionals','aditionals','2017-11-01 03:14:47','2017-11-01 03:14:47',NULL),(83,'add_aditionals','aditionals','2017-11-01 03:14:47','2017-11-01 03:14:47',NULL),(84,'delete_aditionals','aditionals','2017-11-01 03:14:47','2017-11-01 03:14:47',NULL),(85,'browse_saler_users','saler_users','2017-11-10 02:27:32','2017-11-10 02:27:32',NULL),(86,'read_saler_users','saler_users','2017-11-10 02:27:32','2017-11-10 02:27:32',NULL),(87,'edit_saler_users','saler_users','2017-11-10 02:27:32','2017-11-10 02:27:32',NULL),(88,'add_saler_users','saler_users','2017-11-10 02:27:32','2017-11-10 02:27:32',NULL),(89,'delete_saler_users','saler_users','2017-11-10 02:27:32','2017-11-10 02:27:32',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-08 00:55:36','2017-10-08 00:55:36'),(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-08 00:55:36','2017-10-08 00:55:36'),(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-08 00:55:36','2017-10-08 00:55:36'),(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-08 00:55:36','2017-10-08 00:55:36');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2017-10-08 00:55:34','2017-10-08 00:55:34'),(2,'user','Normal User','2017-10-08 00:55:34','2017-10-08 00:55:34'),(3,'saler','Saler','2017-11-10 02:10:23','2017-11-10 02:10:23');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saler_users`
--

DROP TABLE IF EXISTS `saler_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saler_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `saler_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saler_users`
--

LOCK TABLES `saler_users` WRITE;
/*!40000 ALTER TABLE `saler_users` DISABLE KEYS */;
INSERT INTO `saler_users` VALUES (4,3,2,'2017-11-10 04:12:49','2017-11-10 04:12:49'),(5,3,4,'2017-11-10 04:26:51','2017-11-10 04:26:51');
/*!40000 ALTER TABLE `saler_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',1,'pt','Post','2017-10-08 00:55:37','2017-10-08 00:55:37'),(2,'data_types','display_name_singular',2,'pt','Página','2017-10-08 00:55:37','2017-10-08 00:55:37'),(3,'data_types','display_name_singular',3,'pt','Utilizador','2017-10-08 00:55:37','2017-10-08 00:55:37'),(4,'data_types','display_name_singular',4,'pt','Categoria','2017-10-08 00:55:37','2017-10-08 00:55:37'),(5,'data_types','display_name_singular',5,'pt','Menu','2017-10-08 00:55:37','2017-10-08 00:55:37'),(6,'data_types','display_name_singular',6,'pt','Função','2017-10-08 00:55:37','2017-10-08 00:55:37'),(7,'data_types','display_name_plural',1,'pt','Posts','2017-10-08 00:55:37','2017-10-08 00:55:37'),(8,'data_types','display_name_plural',2,'pt','Páginas','2017-10-08 00:55:37','2017-10-08 00:55:37'),(9,'data_types','display_name_plural',3,'pt','Utilizadores','2017-10-08 00:55:37','2017-10-08 00:55:37'),(10,'data_types','display_name_plural',4,'pt','Categorias','2017-10-08 00:55:37','2017-10-08 00:55:37'),(11,'data_types','display_name_plural',5,'pt','Menus','2017-10-08 00:55:37','2017-10-08 00:55:37'),(12,'data_types','display_name_plural',6,'pt','Funções','2017-10-08 00:55:37','2017-10-08 00:55:37'),(13,'categories','slug',1,'pt','categoria-1','2017-10-08 00:55:37','2017-10-08 00:55:37'),(14,'categories','name',1,'pt','Categoria 1','2017-10-08 00:55:37','2017-10-08 00:55:37'),(15,'categories','slug',2,'pt','categoria-2','2017-10-08 00:55:37','2017-10-08 00:55:37'),(16,'categories','name',2,'pt','Categoria 2','2017-10-08 00:55:37','2017-10-08 00:55:37'),(17,'pages','title',1,'pt','Olá Mundo','2017-10-08 00:55:37','2017-10-08 00:55:37'),(18,'pages','slug',1,'pt','ola-mundo','2017-10-08 00:55:37','2017-10-08 00:55:37'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2017-10-08 00:55:37','2017-10-08 00:55:37'),(20,'menu_items','title',1,'pt','Painel de Controle','2017-10-08 00:55:37','2017-10-08 00:55:37'),(21,'menu_items','title',2,'pt','Media','2017-10-08 00:55:37','2017-10-08 00:55:37'),(22,'menu_items','title',3,'pt','Publicações','2017-10-08 00:55:37','2017-10-08 00:55:37'),(23,'menu_items','title',4,'pt','Utilizadores','2017-10-08 00:55:37','2017-10-08 00:55:37'),(24,'menu_items','title',5,'pt','Categorias','2017-10-08 00:55:37','2017-10-08 00:55:37'),(25,'menu_items','title',6,'pt','Páginas','2017-10-08 00:55:37','2017-10-08 00:55:37'),(26,'menu_items','title',7,'pt','Funções','2017-10-08 00:55:37','2017-10-08 00:55:37'),(27,'menu_items','title',8,'pt','Ferramentas','2017-10-08 00:55:38','2017-10-08 00:55:38'),(28,'menu_items','title',9,'pt','Menus','2017-10-08 00:55:38','2017-10-08 00:55:38'),(29,'menu_items','title',10,'pt','Base de dados','2017-10-08 00:55:38','2017-10-08 00:55:38'),(30,'menu_items','title',13,'pt','Configurações','2017-10-08 00:55:38','2017-10-08 00:55:38');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/default.png','$2y$10$XIY3EsjOSrhpV7.LGbtydeWt/13Cszuv8NU88TK29g5zBA45ybGrq','Q2fsL5eI23Yb9HGS7so5QEXzOJZ9Gjz1BtW0yyXweZr2zpjdnw1hIk9g3DrF','2017-10-08 00:55:36','2017-10-08 00:55:36'),(2,2,'Usuario','user@user.com','users/default.png','$2y$10$yd76XCZn9qV6MDBhi.Pjxe9qDSYa6hGoiqsEiXLjY/7DHLr1yus5m','sptqbrDtws0Be5y5u5kmHooKDiN5ghC0lYFuPcrI1i4wwbKDpd7wIGtWnlHx','2017-10-08 01:45:48','2017-10-08 01:45:48'),(3,3,'antoncarbaro','anton@yo.com','users/default.png','$2y$10$aqq/Ri6OsG8ytevqh2usdu0KmqP9Mx0bpWOnJEbseYbIWAgGm3l7e','j2gEUAukKbaiZvzg3UJEP9VuOM2NwnpEMUVrTh62R9rvHObgxA7232PO9Q2q','2017-11-10 01:10:57','2017-11-10 01:10:57'),(4,2,'Sergio Chivito','sergio@chivito.com','users/default.png','$2y$10$pNNzcC0u3qMzsJ/fO7Jtx.wRw4wRv0rCvjAbg1mDUtQri0/D..wdm',NULL,'2017-11-10 04:25:44','2017-11-10 04:25:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(2) NOT NULL DEFAULT '1',
  `seo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `precio_soles` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `precio_dolares` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `precio_integer` int(11) NOT NULL,
  `precio_string` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cilindrada` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `transmision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `velocidades` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `potencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `par_neto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modelo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES (1,'Polo Sedán Comfortline 1.6 Mec',0,'Polo-Sedán-Comfortline-16-Mec',' 55,166','15,990',15990,'','','1,598','Mec','5','105 / 5250','15.6   / 3750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(2,'Golf Comfortline 1.6 Auto',1,'Golf-Comfortline-1.6-auto','70,317','20,990',20990,'','','1.6','Tip','6 ','110 / 5800','155/ 3800 - 4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',4),(3,'Golf comfortline 1.4 TSI Mec',1,'Golf-comfortline-1.4-TSI-Mec','73,667','21,990',21990,'','','1.4','Mec','6','149/5000-6000','250/1500-3500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',4),(4,'Golf comfortline 1.4 TSI DSG',1,'Golf-comfortline-1.4-TSI-DSG','80,367','23,990',23990,'','','1.4','DSG','7','149/5000-6000','250/1500-3500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',4),(5,'Golf GTI 2.0 T DSG',1,'Golf-GTI-2.0-T-DSG','107,167','31,990',31990,'','','2','DSG','6','220/ 4500 - 6200','350/ 1600 - 4200',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',5),(6,'Jetta Trendline 2.0 Mec',1,'Jetta-Trendline-2.0-Mec','56,917','16,990',16990,'','','2','Mec','5','115/5200','170/4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',6),(7,'Jetta Trendline 2.0 Tip',1,'Jetta-Trendline-2.0-Tip','63,617','18,990',18990,'','','2','Tip','6','115/5200','170/4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',6),(8,'Jetta Highline 2.5 Mec',1,'Jetta-Highline-2.5-Mec','70,317',' 20,990',20990,'Sistema de cámara marcha atrás.','','2.5','Mec','5','170/5700','240/4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',6),(9,'Jetta Highline 2.5 Tip',1,'Jetta-Highline-2.5-Tip','77,017','22,990',22990,'Sistema de cámara marcha atrás.','','2.5','Tip','6','170/5700','240/4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',6),(10,'Jetta GLI GP 2.0T Mec',1,'Jetta-GLI-GP-2.0T-Mec','90,417','26,990',26990,'Aros de aluminio de 17 pulgadas.','','2','Mec','6','210/5300 -6200','280/1700 -5200',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',7),(11,'Jetta GLI GP 2.0T DSG',1,'Jetta-GLI-GP-2.0T-DSG','97,117','28,990',28990,'Aros de aluminio de 17 pulgadas.','','2','DSG','6','210/5300 -6200','280/1700 -5200',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',7),(12,'Trendline 1.4 TSI',1,'Trendline_14_TSI','92,092','27,490',27490,'','','1395','DSG','6','150 (110) / 5000 – 6000','250 (26) / 1500 – 3500',NULL,'2017-07-07 20:40:00','2017-10-09 13:28:27',11),(13,'Crossfox Highline 1.6 Mec',1,'Crossfox-Highline-1.6-Mec','53,265','15,900',15900,'Aros de aluminio de 15 pulgadas','','1.6','Mec','6','110/ 5800','160/ 4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',8),(14,'Crossfox Highline 1.6 Auto',1,'Crossfox-Highline-1.6-Auto','58,625','17,500',17500,'Aros de aluminio de 15 pulgadas.','','1.6','Auto','5','110/ 5800','160/ 4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',8),(15,'Tiguan Track & Field 2.0T Mec ',1,'Tiguan-Track&Field-2.0T-Mec','103,466','29,990',29990,'Seguro para niños oculto en puertas traseras.','','2','Mec','6','180/4500','280/1700',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(16,'Tiguan Track & Field 2.0T DSG ',1,'tiguan-track&field-2.0T-DSG','110,517','32,990',32990,'Aros de aluminio de 17 pulgadas.','','2','DSG','7','180/4500','280/1700',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',12),(17,'Tiguan Track & Style 2.0T DSG',1,'Tiguan-Track&Style-2.0T-DSG','130,376','37,790',37790,'Seguro para niños oculto en puertas traseras.','','2','DSG','7','210/5300','280/1700',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(18,'Touareg V6 3.0 TDI',1,'touareg-V6-3.0-tdi','250,915','74,900',74900,'','','3','Tip','8','245/ 4000 - 4400','550/1750-2250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',13),(19,'Caddy',1,'Caddy-Panel','58,592','17,490',17490,'Construcción del chasis frontal absorbente de energía.','','1.6','Mec','5','110/5800','155/3800-4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',16),(20,'Transporter',1,'Transporter-Panel','97,117','28,990',28990,'Radio AM / FM con reproductor de CD, MP3, AUX, USB.','','2','Mec','5','102/3500','250/1500-2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',15),(21,'Crafter 15+1 sin asientos',1,'Crafter-15+1-sin-asientos','155,216','44,990',44990,'Radio AM / FM con reproductor de CD, MP3, AUX, USB.','','2','Mec','6','163/3600','400/1800',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',17),(22,'Crafter 15+1 con asientos',1,'Crafter-15+1-con-asientos','164,117','48,990',48990,'Bolsa de aire para piloto.','','2','Mec','6','163/3600','400/1800',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',17),(23,'Crafter 20+1 sin asientos',1,'Crafter-20+1-sin-asientos','185,892','55,490',55490,'Bolsa de aire para piloto.','','2','Mec','6','163/3600','400/1800',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',17),(24,'Crafter 20+1 con asientos',1,'Crafter-20+1-con-asientos','200,967','59,990',59990,'Sistema de frenos ABS y antideslizamiento de tracción ASR.','','2','Mec','6','163/3600','400/1800',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',17),(25,'Take Up! 1.0 Mec',0,'Take-Up!-1.0-Mec','33.431','9,690',9690,'','','0,999','Mec ','5','75 (55) /6200','95 (9.7) /3000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(26,'Move Up! Mec',0,'Move-Up!-Mec','37,916','10,990',10990,'','','0,999','Mec ','5','75 (55) /6200','95 (9.7) /3000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(27,'High Up! 1.0 Mec',0,'High-Up!-1.0-Mec','42,056','12,190',12190,'','','0,999','Mec ','5','75 (55) /6200','95 (9.7) /3000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(28,'Gol Power 1.6 Auto',1,'Gol-Power-1.6-Auto','47,570','14,200',14200,'','','1,598','Auto','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',2),(29,'Gol Estilo 1.6 Mec',1,'Gol-Estilo-1.6-Mec','46,867','13,990',13990,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',2),(30,'Gol Power 1.6 Mec',1,'Gol-Power-1.6-Mec','42,177','12,590',12590,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',2),(31,'Gol Sedán Concept 1.6 Mec',1,'Auto-Gol-Sedán-Concept-1.6-Mec','36,147','10,790',10790,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',3),(32,'Gol Sedán Power 1.6 Mec',1,'Auto-Gol-Sedán-Power-1.6-Mec','43,182','12,890',12890,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',3),(33,'Gol Sedán Power 1.6 Auto',1,'Auto-Gol-Sedán-Power-1.6-Auto','48,542','14,490',14490,'','','1,598','Auto','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',3),(34,'HIGHLINE BiTDI 4x4 Auto',0,'highline-bitdi-4x4-auto','150,041','43,490',43490,'','','2','Auto','8','180 / 4,000','42.8 / 1,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(35,'HIGHLINE BiTDI 4x4',0,'highline-bitdi-4x4','137,966','39,990',39990,'','','2','Mec','6','180 / 4,000','40.8 / 1,500 a 2,250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(36,'POWER 2.0 TDI 4x4 ',0,'power-2.0-tdi-4x4','105,191','30,490',30490,'','','2','Mec','6','140 / 3,500','34.7 / 1,600 a 2,250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(37,'POWER PLUS 2.0 BiTDI 4x4 ',0,'power-plus-2.0-bitdi-4x4','110,366','31,990',31990,'','','2','Mec','6','180 / 4000','40.8 / 1,500 a 2,250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(38,'POWER PLUS 2.0 TDI 4X4 ',0,'power-plus-2.0-tdi4x4','110,366','31,990',31990,'','','2','Mec','6','140 / 3,500','34.7 / 1,600 a 2,250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(39,'TRENDLINE BiTDI 4x4 Auto',0,'trendline-bitdi-4x4-auto','124,856','36,190',36190,'','','2','Auto','8','180 / 4000','42.8 / 1,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(40,'TRENDLINE BiTDI 4x4',0,'trendline-bitdi-4x4','119,681','34,690',34690,'','','2','Mec','6','180 / 4000','40.8 / 1,500 a 2,250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(41,'BEETLE Design 2.5 Trip',0,'beetle-design-2.5-trip','66,266','19,490',19490,'','','2.48','Tip','6','170 / 5700','240 / 4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(42,'Beetle Design 2.5 Mec',0,'beetle-design-2.5-mec','75,866','21,990',21990,'','','2.48','Mec','5','170 / 5700','240 / 4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(43,'BEETLE Sport 2.5 Trip',0,'beetle-sport-2.5-trip','81,566','23,990',23990,'','','2.48','Tip','6','170 / 5700\n','240 / 4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',0),(44,'Polo Trendline 1.6 Mec',1,'Polo-Trendline-1.6-Mec','55,912','16,690',16690,'','','1,598','Mec','5','105 / 5250','15.6   / 3750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',9),(45,'Gol sedán Comfort 1.6 Mec',1,'Auto-gol-sedán-Comfort-1.6-Mec','44,857','13,390',13390,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',3),(46,'Touareg V6 3.6 TIP',1,'touareg-V6-3.6-tip','217,717','64,990',64990,'','','3.6','Tip','8','245/ 4000 - 4400','550/1750-2250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',13),(47,'Golf Comfortline 1.6 mec',1,'Golf-Comfortline-1.6-mec','63,617','18,990',18990,'','','1.6','Mec','5','110 / 5800','155 / 3800 - 4000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',4),(48,'Beetle Design 1.4 TSI',1,'beetle-design-1.4-tsi','73,667','21,990',21990,'','','1.4','Mecánica','6','170 / 5700','240 / 4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',10),(49,'Beetle Design 1.4 TSI',1,'beetle-design-1.4-tsi','80,367','23,990',23990,'','','1.4','DSG','6','170 / 5700','240 / 4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',10),(50,'Beetle Sport 1.4 TSI',1,'beetle-sport-1.4-tsi','90,417','26,990',26990,'','','1.4','DSG','6','170 / 5700','240 / 4250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',10),(51,'Trendline 2.0 TDI 4x4 ',1,'trendline-2-tdi','92,092','27,490',27490,'','','1.968','Mecánica','6','140 / 3,500','34.7 / 1,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(52,'Trendline Plus 2.0 TDI 4x4',1,'trendline-plus-2-tdi ','110,517','32,990',32990,'','','1.968','Mecánica','6','140 / 3,500','34.7 / 1,750 a 2,500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(53,'Comfortline 2.0 BiTDI 4x4',1,'comfortline-2','120,567','35,990',35990,'','','1.968','Mecánica','6','180 / 4,000','40.8 / 1,500 a 2,250',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(54,'Comfortline 2.0 BiTDI 4x4 AT',1,'comfortline-2-at','125,592','37,490',37490,'','','1.968','Automático','8','180 / 4,000','42.8 / 1,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(55,'Highline 2.0 BiTDI 4x4',1,'highline-2-bitdi','137,317','40,990',40990,'','','1.968','Mecánica','6','180 / 4,000','40.8 / 1,500 a 2,250\r\n',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(56,'Highline 2.0 BiTDI 4x4 AT',1,'highline-2-bitdi-at','144,017','42,990',42990,'','','1.968','Automático','8','180 / 4,000','42.8 / 1,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(57,'3.0 V6 TDI CD 4x4 - AT',1,'3.0-v6-tdi-at','167,467','49,990',49990,'','','2.967','Automático','8','224 (165) / 3,000 a 4,500','550 (56) / 1,400 a 2,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(58,'3.0 V6 TDI CD 4x4 - AT Extreme',1,'3.0-v6-tdi-extreme','184,217','54,990',54990,'','','2.967','Automático','8','224 (165) / 3,000 a 4,500','550 (56) / 1,400 a 2,750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',14),(59,'Spacefox Trendline 1.6 Mec',1,'spacefox-trendline','65,292','19,490',19490,'','','1.6','Mecánico','5','101(74) / 5,250','143(14.6) / 2,500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',20),(60,'Spacefox Highline 1.6',1,'spacefox-highline','73,667','21,990',21990,'','','1.6','ASG','5','110(81) / 5,800','160(16.3) / 4,000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',20),(61,'Polo Sedán Highline 1.6',1,'polo-sedan-highline-mec',' 56,917','16,990',16990,'','','1,598','Mec','5','105 / 5250','15.6   / 3750',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',19),(62,'Gol Sedán Concept Plus 1.6 Mec',1,'gol-sedan-concept-plus-1.6-Mec','39,497','11,790',11790,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',3),(63,'Gol Concept Plus 1.6 Mec',1,'gol-concept-plus-1.6-Mec','38,492','11,490',11490,'','','1,598','Mec','5','101/5250','14.6 / 2500',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',2),(64,'Nuevo Take Up! 2018 Mec',1,'take-up-1.0-mec','33,467','9,990',9990,'','','0,999','Mec ','5','75 (55) /6200','95 (9.7) /3000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',1),(65,'Nuevo Move Up! 2018 Mec',1,'move-up-mec','37,822','11,290',11290,'','','0,999','Mec ','5','75 (55) /6200','95 (9.7) /3000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',1),(66,'Nuevo High Up! 2018 Mec',1,'high-up-Mec','41,842','12,490',12490,'','','0,999','Mec ','5','75 (55) /6200','95 (9.7) /3000',NULL,'2017-07-07 10:40:00','2017-07-07 10:40:00',1),(67,' Comfortline 2.0 TSI 4Motion',1,'Comfortline_2_TSI_4Motion','105,492','31,490',31490,'','','1984','DSG','7','180 (132) / 3940 - 6000','320 (33) / 1500 – 3940',NULL,'2017-10-09 15:31:40','2017-10-09 15:55:15',11),(68,'Highline 2.0 TSI 4Motion',1,'Highline_2_TSI_4Motion','122,242','36,490',36490,'','','1984','DSG','7','180 (132) / 3940 - 6000','320 (33) / 1500 - 3940',NULL,'2017-10-09 15:54:21','2017-10-09 15:54:21',11),(69,'Cross Up!',1,'Cross_Up','44,522','13,290',13290,'','','999','DSG','6','75 /6200','95 /3000',NULL,'2017-10-09 16:17:41','2017-10-09 16:17:41',1);
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-09 18:32:24
