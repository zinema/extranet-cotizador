@extends('flotas.layout.master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div id="btnAccion">
			<a href="{{ route('flotas.new.orden') }}" class="btn btn-warning btn-fill btn-wd" >Nueva cotización</a>
		</div>
		<br>
	</div>
	<div class="col-md-12">
		<div class="card">
			<div class="header">
				<div class="pull-right">
					<a  href="{{ route('flotas.dashboard.descargar.ordenes') }}" class="btn btn-info btn-fill btn-wd">
						Descargar
					</a>
				</div>
				<h4 class="title">Últimas cotizaciones</h4>
				<p class="category">Here is a subtitle for this table</p>
			</div>
			<div class="content table-responsive table-full-width">
				@if(count($ordenes)>0)
				<table class="table">
					<thead> 
						<tr> 
							<th>Orden</th> 
							<th>Fecha</th> 
							<th>Cliente</th>
							<th>Unidades</th>
							<th>Modelo</th>
							<th>Precio ($)</th>
							<th>Estado</th>
							<th>php aVendedor</th>
							<th>Descargar</th>
						</tr>
					</thead> 
					<tbody> 
						@foreach($ordenes as $orden)
						<?php $usuario = App\User::find($orden->user_id); ?>
						<tr>
							<td># {{$orden->id}}</td> 
							<td>{{ $orden->created_at }}</td> 
							<td>{{ $orden->contacto_apellidos }}, {{ $orden->contacto_nombres }} </td>
							<td>{{ $orden->cantidad }}</td>
							<td>{{ $orden->modelo_id }}</td>
							<td>{{ $orden->precio_lista_dolares}}</td>
							<td>Seguimiento</td>
							<td>{{ $usuario['name'] }}</td>
							<td style="text-align: center"><a href="{{ route('cotizaciones.download.pdf', encrypt($orden->pdf)) }}"><i class="fa fa-download" aria-hidden="true"></i></a></td>
						</tr> 
						@endforeach
					</tbody> 
				</table>
				@else
				<h4>No hay cotizaciones</h4>
				@endif
			</div>
		</div>
	</div>
</div>
@stop